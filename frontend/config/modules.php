<?php
return [
    'core' => [
        'class' => 'frontend\modules\core\Module',
    ],
    'user' => [
        'class' => 'frontend\modules\user\Module',
    ],
    'dictionary' => [
        'class' => 'frontend\modules\dictionary\Module',
    ],
    'stock' => [
        'class' => 'frontend\modules\stock\Module'
    ],
    'page' => [
        'class' => 'frontend\modules\page\Module',
    ],
];
?>