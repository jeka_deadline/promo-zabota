<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\modules\user\models\Notification;
use frontend\modules\core\models\Helper;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'My Company',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Правила', 'url' => ['/core/index/rules']],
        ['label' => 'Вопросы и ответы', 'url' => ['/dictionary/faq/index']],
        ['label' => 'Призы', 'url' => '#'],
        ['label' => 'Победители', 'url' => ['/stock/check/winners']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Signup', 'url' => '#', 'linkOptions' => ['class' => 'link-get-modal', 'data' => ['url' => ['/user/security/signup']]]];
        $menuItems[] = ['label' => 'Login', 'url' => '#', 'linkOptions' => ['class' => 'link-get-modal', 'data' => ['url' => ['/user/security/login']]]];
    } else {
        $menuItems[] = ['label' => 'Уведомления(' . Notification::find()->where(['user_id' => Yii::$app->user->identity->getId(), 'is_new' => 1])->count() . ')', 'url' => '#', 'linkOptions' => ['class' => 'link-get-modal', 'data' => ['url' => ['/user/user/get-notifications']]]];
        $menuItems[] = ['label' => 'Личный кабинет', 'url' => ['/user/user/cabinet']];
        $menuItems[] = '<li>'
            . Html::beginForm(['/user/security/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->email . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<div id="modals">
    <?php Modal::begin(); ?>
    <?php Modal::end(); ?>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
