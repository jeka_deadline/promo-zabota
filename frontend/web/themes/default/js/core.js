$(function() {

    $(document).on('submit', 'form.ajax-form', function( e ) {
        var self = $(this);
        var modal = $(this).closest('.modal-body');
        var data = new FormData($(this).get(0));
        e.preventDefault();
        $.ajax({
            url: $(this).attr('action'),
            data: data,
            dataType: 'JSON',
            method: $(this).attr('method'),
            contentType: false,
            cache: false,
            processData: false
        }).done(function(data) {
            if (data.form) {
                $(self).replaceWith(data.form);
            }
            if (data.modal) {
                hideModal();
                $('.modal').replaceWith(data.modal);
                $('.modal').modal('show');
            }
            if (data.reload) {
                setTimeout(function(){location.reload();}, 2000);
            }
        })
    });

    $(document).on('change', '#js-change-sms-code-status', function(e) {
        $('#wrapper-block-input-code-sms').toggleClass('hide');
    });

    $(document).on('click', '.link-get-modal', function( e ) {
        e.preventDefault();
        $.ajax({
            'url': $(this).data('url'),
            'method': 'GET',
            'dataType': 'JSON',
        }).done(function(data) {
            var isViewModal = $(document).find(".modal").hasClass("in");

            if (data.html != '') {
                if (isViewModal) {
                    $(document).find('.modal-backdrop').remove();
                    $(document).find(".modal").modal('hide')
                }
                $(document).find('.modal').replaceWith(data.html);
            }
            $('.modal').modal();
        });
    });

    $(document).on('click', '#link-reset-new-notifications', function(e) {
        e.preventDefault();
        $.ajax({
            url: $(this).data('url'),
            method: 'POST',
        }).done(function() {
            console.log('asdds');
        })
    });

    $(document).on('change', '#js-select-subject-support', function(e) {
        if ($(this).val() == '1' ) {
            $(document).find('#wrapper-custom-subject').removeClass('hide');
        } else {
            $(document).find('#wrapper-custom-subject').addClass('hide');
        }
    });

})

function hideModal()
{
    $('.modal-backdrop').remove();
    $('.modal').modal('hide');
}