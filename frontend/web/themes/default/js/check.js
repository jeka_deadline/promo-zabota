$(document).on('submit', '#register-form-step2', function(e) {
        e.preventDefault();
        $(this).yiiActiveForm('validate');

        if ($(this).find('.has-error').length) {
            return false;
        }

        var self = $(this);
        var data = new FormData($(this).get(0));

        $.ajax({
            url: $(this).attr('action'),
            data: data,
            dataType: 'JSON',
            method: $(this).attr('method'),
            contentType: false,
            cache: false,
            processData: false
        }).done(function(data) {
            if (data.form) {
                $(self).replaceWith(data.form);
            }

            if (data.modal) {
                hideModal();
                $('.modal').replaceWith(data.modal);
                $('.modal').modal('show');
            }

            if (data.reload) {
                setTimeout(function(){location.reload();}, 2000);
            }
        })
    });
