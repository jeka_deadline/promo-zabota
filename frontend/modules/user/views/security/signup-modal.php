<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin(['id' => 'modal-form-default']); ?>

<?php if ($model->scenario !== 'default') : ?>

    <?= yii\authclient\widgets\AuthChoice::widget([
         'baseAuthUrl' => ['/user/security/auth'],
         'popupMode' => TRUE,
    ]) ?>

    <hr>

<?php endif; ?>

<?= $this->render('signup-form', [
    'model' => $model,
    'action' => $action,
]); ?>

<?php Modal::end(); ?>