<?php
use frontend\modules\user\models\forms\CheckUserAgeForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use frontend\modules\stock\models\Pet;
use frontend\modules\core\widgets\Kladr\Kladr;
?>

<?php $form = ActiveForm::begin([
    'id' => 'form-signup',
    'options' => [
        'class' => 'ajax-form'
    ],
    'action' => $action,
]); ?>

    <?= $form->field($model, 'surname')->textInput() ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'email')->widget(MaskedInput::className(), [
        'clientOptions' => [
            'alias' => 'email',
        ],
    ]); ?>

    <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
        'mask' => '7 (999) 999-9999',
    ]); ?>

    <?= $form->field($model, 'dayBirth')->textInput() ?>

    <?= $form->field($model, 'monthBirth')->dropDownList(CheckUserAgeForm::getListMonths()); ?>

    <?= $form->field($model, 'yearBirth')->textInput(); ?>

    <?= $form->field($model, 'city')->widget(Kladr::className(), [
        'defaultOptions' => [
            'type' => Kladr::TYPE_CITY,
        ],
        'options' => [
            'class' => 'form-control',
        ],
    ]) ?>

    <?php //if ($model->scenario !== 'default') : ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'hasCodeSms')->checkbox(['id' => 'js-change-sms-code-status']); ?>

            <?= Html::beginTag('div', [
                'id' => 'wrapper-block-input-code-sms',
                'class' => (!$model->hasCodeSms) ? 'hide' : NULL,
            ]); ?>

                <?= $form->field($model, 'codeSms')->textInput(['id' => 'input-sms-code']); ?>

            <?= Html::endTag('div'); ?>

    <?php //endif; ?>

    <?= $form->field($model, 'petId')->radioList(Pet::getDropdownListPets()); ?>

    <?= $form->field($model, 'rules')->checkbox(); ?>

    <?= $form->field($model, 'politics')->checkbox(); ?>

    <div class="form-group">

        <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>

    </div>

<?php ActiveForm::end(); ?>