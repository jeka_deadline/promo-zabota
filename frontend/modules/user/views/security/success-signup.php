<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin([
    'header' => 'Успешная регистрация',
]); ?>

Вы успешно зарегистрировались на сайте, на Ваш электронный адрес было отправлено письмо для подтверждения Вашего email.

<?php Modal::end(); ?>
