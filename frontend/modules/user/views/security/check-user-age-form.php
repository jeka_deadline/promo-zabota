<?php
use frontend\modules\user\models\forms\CheckUserAgeForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin(['options' => ['class' => 'ajax-form'], 'action' => ['/user/security/check-user-age']]); ?>

    <?= $form->field($model, 'dayBirth'); ?>

    <?= $form->field($model, 'monthBirth')->dropDownList(CheckUserAgeForm::getListMonths()); ?>

    <?= $form->field($model, 'yearBirth'); ?>

    <?= Html::submitButton('Продолжить', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>