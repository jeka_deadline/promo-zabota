<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => [
        'class' => 'ajax-form'
    ],
    'action' => ['/user/security/login'],
]); ?>

    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'rememberMe')->checkbox() ?>

    <div style="color:#999;margin:1em 0">
        <?= Html::a('зарегистрироваться', '#', ['class' => 'link-get-modal', 'data' => ['url' => ['/user/security/signup']]]) ?>
        <?= Html::a('или забыли пароль?', ['/user/security/request-password-reset']) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </div>

<?php ActiveForm::end(); ?>
