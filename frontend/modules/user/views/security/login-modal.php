<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin(['id' => 'modal-form-default']); ?>


    <?= yii\authclient\widgets\AuthChoice::widget([
        'baseAuthUrl' => ['/user/security/auth'],
        'popupMode' => TRUE,
    ]) ?>

    <hr>


    <?= $this->render('login-form', [
        'model' => $model,
    ]); ?>

<?php Modal::end(); ?>