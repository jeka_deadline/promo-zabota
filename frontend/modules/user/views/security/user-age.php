<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin(['id' => 'modal-form-default']); ?>

    <?= $this->render('user-age-form', [
        'model' => $model,
    ]); ?>

<?php Modal::end(); ?>