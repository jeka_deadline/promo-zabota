<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'ajax-form'
    ],
    'action' => ['/user/user/bind-phone'],
]); ?>

    <?= $form->field($model, 'phone')->widget(MaskedInput::className(), [
        'mask' => '+7 (999) 999-9999',
    ]); ?>

    <?= Html::submitButton('Далее', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>