<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\modules\stock\models\Pet;
?>

<?php $form = ActiveForm::begin([
    //'action' => ['/user/user/change-user-profile-fields'],
]); ?>

    <?= $form->field($model, 'name')->textinput(['disabled' => 'disabled']); ?>

    <?= $form->field($model, 'surname')->textinput(['disabled' => 'disabled']); ?>

    <?= $form->field($model, 'region'); ?>

    <?= $form->field($model, 'email'); ?>

    <?= $form->field($model, 'city'); ?>

    <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7 (999) 999-9999',
        'options' => [
            'disabled' => 'disabled',
            'class' => 'form-control',
        ],
    ]) ?>

    <?= $form->field($model, 'petId')->radioList(Pet::getDropdownListPets()); ?>

    <a href="#" class="link-get-modal" data-url="<?= Url::toRoute(['/user/user/bind-phone']); ?>">Привязать</a>

    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>