<?php
use yii\widgets\ListView;
use yii\widgets\LinkPager;
?>

<table class="table">
    <tr>
        <th>Название приза</th>
        <th>Дата выиграша</th>
        <th>Номер чека</th>
        <th>Код</th>
        <th>Статус</th>
    </tr>

    <?= ListView::widget([
        'dataProvider' => $userChecksDataProvider,
        'layout' => '{items}',
        'itemView' => 'item_checks_and_prizes',
        'options' => [
            'tag' => FALSE,
        ],
        'itemOptions' => [
            'tag' => FALSE,
        ],
    ]); ?>

</table>

<?= LinkPager::widget([
    'pagination' => $userChecksDataProvider->pagination,
]); ?>