<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'ajax-form'
    ],
    'action' => ['/user/user/finish-bind-phone'],
]); ?>

    <?= $form->field($model, 'code'); ?>

    <?= Html::submitButton('Далее', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>