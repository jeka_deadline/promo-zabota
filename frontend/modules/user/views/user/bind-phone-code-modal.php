<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin([
    'header' => 'Сообщение',
]); ?>

    <?= $this->render('bind-phone-code-form', [
        'model' => $model,
    ]); ?>

<?php Modal::end(); ?>