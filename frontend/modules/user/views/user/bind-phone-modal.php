<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin([
    'header' => 'Сообщение',
]); ?>

    <?= $this->render('bind-phone-modal-form', [
        'model' => $model,
    ]); ?>

<?php Modal::end(); ?>