<?php
use yii\bootstrap\Modal;
use yii\helpers\Url;
?>

<?php Modal::begin(['id' => 'modal-form-default', 'header' => 'Уведомления']); ?>

    <?php if (!$notifications) : ?>

        У Вас нет уведомлений

    <?php else : ?>

      <a href="#" id="link-reset-new-notifications" data-url="<?= Url::toRoute(['/user/user/reset-new-notifications']); ?>">Отметить все как прочитанные</a>

        <?php foreach ($notifications as $notification): ?>

            <p style="border: 1px solid #000">
                <?= $notification->text; ?>
            </p>

        <?php endforeach; ?>

    <?php endif; ?>

<?php Modal::end(); ?>