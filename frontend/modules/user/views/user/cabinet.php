<?php
use yii\bootstrap\Tabs;
$tabName = Yii::$app->request->get('tab');
?>

<?= Tabs::widget([
    'items' => [
        [
            'label' => 'Профайл',
            'content' => $this->render('tab-cabinet-profile', [
                  'model' => $profileModel,
            ]),
            'active' => ((!$tabName) && ($tabName !== 'checks-and-prizes' || $tabName !== 'delivery')),
        ],
        [
            'label' => 'Чеки и призы',
            'content' => $this->render('tab-cabinet-checks-and-prizes', [
                'userChecksDataProvider' => $userChecksDataProvider,
            ]),
            'active' => ($tabName === 'checks-and-prizes'),
        ],
        [
            'label' => 'Доставка призов',
            'content' => $this->render('tab-cabinet-delivery', [
                'listWinnersChecksDataProvider' => $listWinnersChecksDataProvider,
            ]),
            'active' => ($tabName === 'delivery'),
        ],
    ],
]); ?>