<?php
use frontend\modules\user\widgets\UserAddress\UserAddress;
use yii\widgets\ListView;
use yii\widgets\LinkPager;
?>

<?= UserAddress::widget(); ?>

<table class="table">
    <tr>
        <th>Название приза</th>
        <th>Статус</th>
        <th>Номер отслеживания (ШПИ)</th>
        <th></th>
    </tr>
    <tbody>

        <?php if ($listWinnersChecksDataProvider->totalCount) : ?>

            <?= ListView::widget([
                'dataProvider' => $listWinnersChecksDataProvider,
                'itemView' => 'item_winner_check',
                'options' => [
                    'tag' => FALSE,
                ],
                'itemOptions' => [
                    'tag' => FALSE,
                ],
                'layout' => '{items}',
            ]); ?>

        <?php else : ?>

            <tr><td colspan="4">Пусто</td></tr>

        <?php endif; ?>

    </tbody>

</table>

<?= LinkPager::widget([
    'pagination' => $listWinnersChecksDataProvider->pagination,
]); ?>