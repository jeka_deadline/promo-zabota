<?php
namespace frontend\modules\user\models;

use Yii;
use common\models\user\LogProfile as BaseLogProfile;

class LogProfile extends BaseLogProfile
{
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
