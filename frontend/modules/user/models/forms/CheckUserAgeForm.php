<?php
namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;

class CheckUserAgeForm extends Model
{

    public $dayBirth;
    public $monthBirth;
    public $yearBirth;

    public function rules()
    {
        return [
            [['dayBirth', 'monthBirth', 'yearBirth'], 'required'],
            [['dayBirth'], 'number'],
            [['monthBirth'], 'number', 'min' => 1, 'max' => 12],
            [['yearBirth'], 'number', 'max' => date('Y')],
        ];
    }

    public static function getListMonths()
    {
        return [
          '01' => 'Январь',
          '02' => 'Февраль',
          '03' => 'Март',
          '04' => 'Апрель',
          '05' => 'Май',
          '06' => 'Июнь',
          '07' => 'Июль',
          '08' => 'Август',
          '09' => 'Сентябрь',
          '10' => 'Октябрь',
          '11' => 'Ноябрь',
          '12' => 'Декабрь',
        ];
    }

    public function checkUserAge()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $cookies = Yii::$app->response->cookies;

        $age = $this->getAge($this->yearBirth, $this->monthBirth, $this->dayBirth);

        if ($age > 12) {
            $cookies->add(new \yii\web\Cookie([
                'name' => 'check-user-age',
                'value' => serialize(['dayBirth' => $this->dayBirth, 'monthBirth' => $this->monthBirth, 'yearBirth' => $this->yearBirth]),
            ]));
            return TRUE;
        }

        $this->addError('yearBirth', 'Ваш возраст меньше 12 лет');
        return FALSE;
    }

    private function getAge($y, $m, $d)
    {
        if ($m > date('m') || $m == date('m') && $d > date('d')) {
          return (date('Y') - $y - 1);
        } else {
          return (date('Y') - $y);
        }
    }

}