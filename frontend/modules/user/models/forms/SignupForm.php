<?php
namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;
use frontend\modules\user\models\Profile;
use yii\db\Expression;
use frontend\modules\user\models\Social;
use frontend\modules\stock\models\Pet;
use yii\helpers\ArrayHelper;
use frontend\modules\stock\models\Check;

class SignupForm extends Model
{
    public $surname;
    public $name;
    public $dayBirth;
    public $monthBirth;
    public $yearBirth;
    public $city;
    public $email;
    public $phone;
    public $password;
    //public $repeatPassword;
    public $hasCodeSms;
    public $codeSms;
    public $rules;
    public $politics;
    public $petId;

    const SCENARIO_SITE_REGISTER = 'site-register';

    public function rules()
    {
        return [
            [['email', 'surname', 'name', 'codeSms', 'city', 'phone'], 'trim'],
            [['email', 'surname', 'name', 'city', 'rules', 'dayBirth', 'monthBirth', 'yearBirth', 'phone', 'petId'], 'required'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => 'This email has already been taken.'],
            ['email', 'email'],
            ['petId', 'exist', 'targetClass' => Pet::className(), 'targetAttribute' => 'id', 'filter' => function($query) {
                return $query->andWhere(['active' => 1]);
            }],
            [['yearBirth'], 'number', 'max' => date('Y')],
            [['dayBirth', 'monthBirth'], 'safe'],

            [['surname', 'name', 'city'], 'string', 'max' => '100'],

            [['password'], 'required', 'on' => self::SCENARIO_SITE_REGISTER],
            ['password', 'string', 'min' => 6, 'on' => self::SCENARIO_SITE_REGISTER],
            [['hasCodeSms', 'rules'], 'boolean', 'on' => self::SCENARIO_SITE_REGISTER],
            [['codeSms'], 'required', 'when' => function($model){
                return ($model->hasCodeSms);
            }, 'whenClient' => "function (attribute, value) {
                    return $('#js-change-sms-code-status').prop('checked') == true;
                }", 'on' => self::SCENARIO_SITE_REGISTER,
            ],

            ['codeSms', 'exist', 'targetClass' => Check::className(), 'targetAttribute' => 'attach_sms_code', 'when' => function($model){
                return ($model->hasCodeSms);
            }, 'on' => self::SCENARIO_SITE_REGISTER],
            [['rules', 'politics'], 'compare', 'compareValue' => 1],
        ];
    }

    public function signup()
    {
        if (!$this->validate()) {
            return NULL;
        }

        $cookies = Yii::$app->request->cookies;

        if (!$cookies->has('check-user-age')) {
            return FALSE;
        }

        $userDataBirth    = unserialize($cookies->get('check-user-age'));
        $this->dayBirth   = $userDataBirth[ 'dayBirth' ];
        $this->monthBirth = $userDataBirth[ 'monthBirth' ];
        $this->yearBirth  = $userDataBirth[ 'yearBirth' ];

        $user                     = new User();
        $user->email              = $this->email;
        $user->temporary_code = $this->generateTemporaryCode();
        $user->created_at         = new Expression('NOW()');
        $user->updated_at         = new Expression('NOW()');

        $user->setRegisterIp();
        $user->setPassword($this->password);
        $user->generateAuthKey();

        $transaction = User::getDb()->beginTransaction();

        if ($user->save()) {
            $phone = preg_replace('/\D/', '', $this->phone);
            $profile              = new Profile();
            $profile->surname     = $this->surname;
            $profile->name        = $this->name;
            $profile->city        = $this->city;
            $profile->date_birth  = $this->yearBirth . '-' . $this->monthBirth . '-' . $this->dayBirth;
            $profile->phone       = $phone;
            $profile->pet_id      = $this->petId;

            $user->link('profile', $profile);

            if ($this->codeSms) {
                $checks = Check::find()
                                    ->where(['attach_sms_code' => $this->codeSms])
                                    ->andWhere(['is', 'user_id', NULL])
                                    ->all();

                foreach ($checks as $check) {
                    $check->user_id         = $user->id;
                    $check->user_email      = $user->email;
                    $check->user_name       = $profile->surname . ' ' . $profile->name;
                    $check->attach_sms_code = NULL;

                    $check->save(FALSE);
                }
            }

            $this->sendSignupCode($user->temporary_code);

            $transaction->commit();

            return $user;
        }

        return NULL;

    }

    public function signupSocial()
    {
        if (!$this->validate()) {
            return NULL;
        }

        $user                     = new User();
        $user->email              = $this->email;
        $user->login_with_social  = 1;
        $user->confirm_email_at   = new Expression('NOW()');
        $user->created_at         = new Expression('NOW()');
        $user->updated_at         = new Expression('NOW()');

        $user->setRegisterIp();
        $user->setPassword(Yii::$app->security->generateRandomString());
        $user->generateAuthKey();

        $transaction = User::getDb()->beginTransaction();

        if ($user->save()) {
            $profile              = new Profile();
            $profile->surname     = $this->surname;
            $profile->name        = $this->name;
            $profile->city        = $this->city;
            $profile->date_birth  = $this->yearBirth . '-' . $this->monthBirth . '-' . $this->dayBirth;

            $user->link('profile', $profile);

            $session = Yii::$app->session;
            if ($session->has('user-social-provider')) {
                $userSocialProvider = unserialize(base64_decode($session->get('user-social-provider')));
                $session->remove('user-social-provider');

                $social             = new Social();
                $social->provider   = (isset($userSocialProvider[ 'provider' ])) ? $userSocialProvider[ 'provider' ] : NULL;
                $social->client_id  = (isset($userSocialProvider[ 'client_id' ])) ? $userSocialProvider[ 'client_id' ] : NULL;
                $social->created_at = new Expression('NOW()');

                $user->link('social', $social);
            }

            if ($this->codeSms) {
                $checks = Check::find()
                                    ->where(['attach_sms_code' => $this->codeSms])
                                    ->andWhere(['is', 'user_id', NULL])
                                    ->all();

                foreach ($checks as $check) {
                    $check->user_id         = $user->id;
                    $check->user_email      = $user->email;
                    $check->user_name       = $profile->surname . ' ' . $profile->name;
                    $check->attach_sms_code = NULL;

                    $check->save(FALSE);
                }
            }

            $transaction->commit();

            return $user;
        }
    }

    private function sendSignupCode($code)
    {
        Yii::$app->mailer->compose('confirm-email', ['code' => $code])
            ->setFrom([Yii::$app->params[ 'supportEmail' ] => 'from@domain.com'])
            ->setTo($this->email)
            ->setSubject('Confirm email')
            ->send();
    }

    private function generateTemporaryCode()
    {
        while (TRUE) {
            $code = md5(Yii::$app->security->generateRandomString(10) . time());

            $user = User::findOne(['temporary_code' => $code]);

            if (!$user) {
                return $code;
            }
        }
    }
}