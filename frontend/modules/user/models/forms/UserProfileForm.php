<?php
namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;
use frontend\modules\user\models\LogProfile;
use frontend\modules\stock\models\Pet;
use yii\web\NotFoundHttpException;

class UserProfileForm extends Model
{
    public $surname;
    public $name;
    public $region;
    public $email;
    public $city;
    public $phone;
    public $petId;

    public function rules()
    {
        return [
            [['region', 'email', 'city', 'surname', 'name'], 'trim'],
            [['region', 'email', 'city', 'petId'], 'required'],
            ['email', 'unique', 'targetClass' => User::className(), 'message' => 'This email has already been taken.', 'filter' => function($query) {
                return $query->andWhere(['<>', 'id', Yii::$app->user->identity->getId()]);
            }],
            ['email', 'email'],
            [['region', 'city'], 'string', 'max' => 100],
            ['phone', 'safe'],
            ['petId', 'exist', 'targetClass' => Pet::className(), 'targetAttribute' => 'id', 'filter' => function($query) {
                return $query->andWhere(['active' => 1]);
            }],
        ];
    }

    public function changeUserProfileFields($profile)
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $cookies = Yii::$app->request->cookies;

        /*if (!$cookies->has('check-user-age')) {
            return FALSE;
        }*/

        $user = User::find()
                          ->where(['id' => Yii::$app->user->identity->getId()])
                          ->with('profile')
                          ->one();

        if (!$user || !$user->profile) {
            throw new NotFoundHttpException('Пользователь или профиль пользователя не найден');
        }

        $user->profile->attributes  = $this->attributes;
        $user->profile->surname     = $profile->surname;
        $user->profile->name        = $profile->name;
        $user->profile->phone       = $profile->phone;
        $user->profile->pet_id      = $this->petId;

        if (!$user->profile->validate() || !$user->profile->save()) {
            return FALSE;
        }

        $logProfile = new LogProfile();
        $logProfile->attributes = $user->profile->attributes;

        $logProfile->save();

        return TRUE;
    }

    public function initArrtibutes($user)
    {
        $this->setAttributes($user->profile->attributes);

        $this->petId = $user->profile->pet_id;
        $this->email = $user->email;
        $this->phone = ($this->phone) ? substr($this->phone, 1) : NULL;
    }
}