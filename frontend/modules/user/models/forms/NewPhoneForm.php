<?php
namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;
use frontend\modules\user\models\Profile;
use frontend\modules\user\models\LogProfile;

class NewPhoneForm extends Model
{
    public $phone;

    public function rules()
    {
        return [
            [['phone'], 'required'],
        ];
    }

    public function sendCode()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $phone    = preg_replace('/\D/', '', $this->phone);

        if (!preg_match('/^\d{11}$/', $phone)) {
            $this->addError('phone', 'Не правильный номер телефона');

            return FALSE;
        }

        $user     = User::find()
                              ->where(['id' => Yii::$app->user->identity->getId()])
                              ->with('profile')
                              ->one();

        $profile  = Profile::find()
                                ->where(['phone' => $phone])
                                ->with('user')
                                ->one();

        if ($profile) {
            $this->addError('phone', 'Пользователь с таким телефоном уже есть');

            return FALSE;
        }

        $logProfile                     = new LogProfile();
        $logProfile->attributes         = $user->profile->attributes;
        $logProfile->phone              = $phone;
        $logProfile->code_change_phone  = $this->generateTemporaryCode();

        if ($logProfile->validate() && $logProfile->save()) {

            $user->temporary_code = $logProfile->code_change_phone;
            $user->save(FALSE);

            return TRUE;
        }

        return FALSE;
    }

    private function generateTemporaryCode()
    {
        while (TRUE) {
            $code = '';

            for ($i = 0; $i < 5; $i++) {
                $code .= mt_rand(0, 9);
            }

            $user = User::findOne(['temporary_code' => $code]);

            if (!$user) {
                return $code;
            }
        }
    }
}
