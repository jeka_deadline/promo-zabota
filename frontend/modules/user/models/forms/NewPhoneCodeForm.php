<?php
namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;
use frontend\modules\user\models\LogProfile;

class NewPhoneCodeForm extends Model
{
    public $code;

    public function rules()
    {
        return [
            [['code'], 'required'],
        ];
    }

    public function changeUserPhone()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $userId = Yii::$app->user->identity->getId();

        $user = User::find()
                        ->where(['id' => $userId, 'temporary_code' => $this->code])
                        ->with('profile')
                        ->one();

        $logProfile = LogProfile::find()
                                    ->where(['user_id' => $userId, 'code_change_phone' => $this->code])
                                    ->one();


        if (!$user || !$logProfile) {
            $this->addError('code', 'Пользователь с таким кодом не найден');

            return FALSE;
        }

        $user->profile->phone = $logProfile->phone;
        $user->temporary_code = NULL;

        $user->profile->save(FALSE);
        $user->save(FALSE);

        return TRUE;
    }
}