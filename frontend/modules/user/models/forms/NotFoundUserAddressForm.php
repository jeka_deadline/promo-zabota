<?php
namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;

class NotFoundUserAddressForm extends Model
{

    public $address;
    public $captcha;

    public function rules()
    {
        return [
            [['address'], 'required'],
            [['address'], 'filter', 'filter' => 'trim'],
            ['captcha', 'captcha', 'captchaAction' => '/core/index/captcha'],
        ];
    }

    public function saveAddress()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $user = User::findOne(Yii::$app->user->identity->getId());

        if (!$user->profile) {
            return FALSE;
        }

        $user->profile->address = $this->address;

        $user->profile->save(FALSE);

        return TRUE;
    }

    public function attributeLabels()
    {
        return [
            'address' => 'Введите адрес, который не удалось найти',
            'captcha' => 'Введите символы с изображения',
        ];
    }

}