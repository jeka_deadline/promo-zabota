<?php
namespace frontend\modules\user\models;

use Yii;
use common\models\user\User as BaseUser;

class User extends BaseUser
{

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    public function generatePasswordResetToken()
    {
        $hash = md5(Yii::$app->security->generateRandomString() . time());

        for(;;) {
            $user = static::find()
                              ->where(['reset_password_token' => $hash])
                              ->one();

            if (!$user) {
                break;
            }

            $hash = md5(Yii::$app->security->generateRandomString() . time());
        }

        $this->reset_password_token = $hash;
    }

    public static function findByPasswordResetToken($token)
    {
        return static::findOne(['reset_password_token' => $token]);
    }

    public function removePasswordResetToken()
    {
        $this->reset_password_token = NULL;
    }

    // ================================================================== Relations =========================================================================

    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    public function getSocial()
    {
        return $this->hasMany(Social::className(), ['user_id' => 'id']);
    }

}