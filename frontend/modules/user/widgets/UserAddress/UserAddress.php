<?php
namespace frontend\modules\user\widgets\UserAddress;

use Yii;
use yii\base\Widget;
use frontend\modules\user\models\User;
use frontend\modules\user\models\forms\UserAddressForm;

class UserAddress extends Widget
{

    public function run()
    {

        $user = User::findOne(Yii::$app->user->identity->getId());

        if (!$user || !$user->profile || $user->profile->address) {
            return FALSE;
        }

        $model = new UserAddressForm();

        return $this->render('block-user-address', [
            'model' => $model,
        ]);

    }

}
?>