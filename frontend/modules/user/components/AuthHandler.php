<?php
namespace frontend\modules\user\components;

use frontend\modules\user\models\Social;
use frontend\modules\user\models\User;
use frontend\modules\user\models\forms\SignupForm;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

/**
 * AuthHandler handles successful authentication via Yii auth component
 */
class AuthHandler
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        $attributes = $this->client->getUserAttributes();

        $id = ArrayHelper::getValue($attributes, 'id');

        $userAttributes = $this->getUserAttributes($attributes);

        $auth = Social::find()->where([
            'provider' => $this->client->getId(),
            'client_id' => $id,
        ])->one();

        if (Yii::$app->user->isGuest) {
            if ($auth) {
                $user = $auth->user;
                Yii::$app->user->login($user);
            } else { // signup
                if ($userAttributes[ 'email' ] !== null && $user = User::find()->where(['email' => $userAttributes[ 'email' ]])->one()) {
                    $social             = new Social();
                    $social->user_id    = $user->id;
                    $social->provider   = $this->client->getId();
                    $social->client_id  = $id;
                    $social->created_at = new Expression('NOW()');

                    $social->save();

                    Yii::$app->user->login($user);
                } else {

                    $model = new SignupForm();
                    $model->setAttributes($userAttributes);

                    if ($model->signupSocial()) {
                        $user = Yii::$app->user->login($user);
                        return NULL;
                    }

                    $userSocialProvider = [
                        'provider'  => $this->client->getId(),
                        'client_id' => $id,
                    ];

                    $session = Yii::$app->session;
                    $session->set('social-user-attributes', base64_encode(serialize($userAttributes)));
                    $session->set('user-social-provider', base64_encode(serialize($userSocialProvider)));
                }
            }
        }
    }

    private function getUserAttributes($attributes)
    {
        $userAttributes = [
            'email'       => NULL,
            'surname'     => NULL,
            'name'        => NULL,
            'city'        => NULL,
            'dayBirth'    => NULL,
            'monthBirth'  => NULL,
            'yearBirth'   => NULL,
        ];

        if ($this->client instanceof \yii\authclient\clients\Facebook) {
            $userAttributes[ 'email' ]    = ArrayHelper::getValue($attributes, 'email');
            $userAttributes[ 'surname' ]  = ArrayHelper::getValue($attributes, 'last_name');
            $userAttributes[ 'name' ]     = ArrayHelper::getValue($attributes, 'first_name');

            $birthday = ArrayHelper::getValue($attributes, 'birthday');

            if ($birthday) {
                $dataBirthday = explode('/', $birthday);

                $userAttributes[ 'dayBirth' ]   = $dataBirthday[ 0 ];
                $userAttributes[ 'monthBirth' ] = $dataBirthday[ 1 ];
                $userAttributes[ 'yearBirth' ]  = $dataBirthday[ 2 ];
            } else {
                $userAttributes = $this->setCookieDateBirth($userAttributes);
            }
        } else if ($this->client instanceof \yii\authclient\clients\Vkontakte) {
            $userAttributes[ 'email' ]    = ArrayHelper::getValue($attributes, 'email');
            $userAttributes[ 'surname' ]  = ArrayHelper::getValue($attributes, 'last_name');
            $userAttributes[ 'name' ]     = ArrayHelper::getValue($attributes, 'first_name');

            $birthday = ArrayHelper::getValue($attributes, 'bdate');

            if ($birthday) {
                $dataBirthday = explode('.', $birthday);

                $userAttributes[ 'dayBirth' ]   = str_pad($dataBirthday[ 0 ], 2, 0);
                $userAttributes[ 'monthBirth' ] = str_pad($dataBirthday[ 1 ], 2, 0);
                $userAttributes[ 'yearBirth' ]  = $dataBirthday[ 2 ];
            } else {
                $userAttributes = $this->setCookieDateBirth($userAttributes);
            }
        } else if ($this->client instanceof \frontend\modules\user\components\clients\Odnoklassniki) {
            $userAttributes[ 'surname' ]  = ArrayHelper::getValue($attributes, 'last_name');
            $userAttributes[ 'name' ]     = ArrayHelper::getValue($attributes, 'first_name');
            $birthday = ArrayHelper::getValue($attributes, 'birthday');

            if ($birthday) {
                $dataBirthday = explode('-', $birthday);

                $userAttributes[ 'dayBirth' ]   = $dataBirthday[ 2 ];
                $userAttributes[ 'monthBirth' ] = $dataBirthday[ 1 ];
                $userAttributes[ 'yearBirth' ]  = $dataBirthday[ 0 ];
            }

            $location = ArrayHelper::getValue($attributes, 'location');

            if ($location && isset($location[ 'city' ])) {
                $userAttributes[ 'city' ] = $location[ 'city' ];
            }
        }

        return $userAttributes;
    }

    private function setCookieDateBirth($attributes)
    {
        $cookies = Yii::$app->request->cookies;

        if (!$cookies->has('check-user-age')) {
            return $attributes;
        }

        $userDataBirth = unserialize($cookies->get('check-user-age'));

        $attributes[ 'dayBirth' ]   = $userDataBirth[ 'dayBirth' ];
        $attributes[ 'monthBirth' ] = $userDataBirth[ 'monthBirth' ];
        $attributes[ 'yearBirth' ]  = $userDataBirth[ 'yearBirth' ];

        return $attributes;
    }
}