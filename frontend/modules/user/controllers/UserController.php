<?php
namespace frontend\modules\user\controllers;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use frontend\modules\user\models\Notification;
use frontend\modules\user\models\User;
use frontend\modules\user\models\forms\UserProfileForm;
use frontend\modules\user\models\forms\NewPhoneForm;
use frontend\modules\user\models\forms\NewPhoneCodeForm;
use yii\data\ActiveDataProvider;
use frontend\modules\stock\models\Check;

class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['get-notifications', 'reset-new-notifications', 'cabinet', 'bind-phone', 'finish-bind-phone', 'change-user-profile-fields'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionGetNotifications()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $notifications = Notification::find()
                                          ->where(['user_id' => Yii::$app->user->identity->getId()])
                                          ->orderBy(['created_at' => SORT_DESC])
                                          ->all();

        $response[ 'html' ] = $this->renderPartial('list-notifications', [
            'notifications' => $notifications,
        ]);

        return $response;
    }

    public function actionResetNewNotifications()
    {
        Notification::updateAll(['is_new' => 0], 'user_id = ' . Yii::$app->user->identity->getId());
    }

    public function actionCabinet()
    {
        $user = User::find()
                          ->where(['id' => Yii::$app->user->identity->getId()])
                          ->with('profile')
                          ->one();

        if (!$user || !$user->profile) {
            throw new NotFoundHttpException('Пользователь или профиль пользователя не найден');
        }

        $profileModel = new UserProfileForm();

        $profileModel->initArrtibutes($user);

        if ($profileModel->load(Yii::$app->request->post()) && $profileModel->changeUserProfileFields($user->profile)) {

            return $this->render('success-save-profile-information');
        }

        $userChecksDataProvider = new ActiveDataProvider([
            'query' => Check::find()
                                  ->where(['user_id' => $user->id]),
            'pagination' => [
                'pageSize' => 10,
                'params' => [
                    'page' => (Yii::$app->request->get('tab') === 'checks-and-prizes') ? Yii::$app->request->get('page') : 1,
                    'tab'  => 'checks-and-prizes',
                ],
            ],
        ]);

        $listWinnersChecksDataProvider = new ActiveDataProvider([
            'query' => Check::find()
                                ->where(['user_id' => $user->id, 'status' => Check::STATUS_ACCEPT])
                                ->andWhere(['not', ['prize_id' => NULL]]),
            'pagination' => [
                'pageSize' => 10,
                'params' => [
                    'page' => (Yii::$app->request->get('tab') === 'delivery') ? Yii::$app->request->get('page') : 1,
                    'tab'  => 'delivery',
                ],
            ],
        ]);

        return $this->render('cabinet', [
            'user'                          => $user,
            'profileModel'                  => $profileModel,
            'userChecksDataProvider'        => $userChecksDataProvider,
            'listWinnersChecksDataProvider' => $listWinnersChecksDataProvider,
        ]);
    }

    public function actionBindPhone()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [
            'html' => NULL,
            'form' => NULL,
        ];

        $model = new NewPhoneForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->sendCode()) {

                $model = new NewPhoneCodeForm();

                $response[ 'modal' ] = $this->renderPartial('bind-phone-code-modal', [
                    'model' => $model,
                ]);
            } else {
                $response[ 'form' ] = $this->renderAjax('bind-phone-modal-form', [
                    'model' => $model,
                ]);
            }

            return $response;
        }

        $response[ 'html' ] = $this->renderAjax('bind-phone-modal', [
            'model' => $model,
        ]);

        return $response;
    }

    public function actionFinishBindPhone()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [
            'html'    => NULL,
            'form'    => NULL,
            'reload'  => FALSE,
        ];

        $model = new NewPhoneCodeForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->changeUserPhone()) {
                $response[ 'form' ] = $this->renderPartial('success-change-phone');
                $response[ 'reload' ] = TRUE;
            } else {
                $response[ 'form' ] = $this->renderPartial('bind-phone-code-form', [
                    'model' => $model,
                ]);
            }

            return $response;
        }

        $response[ 'html' ] = $this->renderPartial('bind-phone-code-modal', [
            'model' => $model,
        ]);
    }
}
