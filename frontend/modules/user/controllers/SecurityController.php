<?php
namespace frontend\modules\user\controllers;

use frontend\modules\user\components\AuthHandler;
use frontend\modules\user\models\forms\CheckUserAgeForm;
use frontend\modules\user\models\User;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Expression;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\modules\user\models\forms\LoginForm;
use frontend\modules\user\models\forms\PasswordResetRequestForm;
use frontend\modules\user\models\forms\ResetPasswordForm;
use frontend\modules\user\models\forms\SignupForm;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class SecurityController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function onAuthSuccess($client)
    {
        (new AuthHandler($client))->handle();

        return $this->redirect(['/core/index/index']);
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }

        $response = [
            'html'    => NULL,
            'form'    => NULL,
            'reload'  => FALSE,
        ];

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                if (Yii::$app->request->isAjax) {
                    $response[ 'html' ] = '';
                    $response[ 'reload' ] = TRUE;

                    return $response;
                }

                $this->goBack();

                return FALSE;
            }

            if (Yii::$app->request->isAjax) {
                $response[ 'form' ] = $this->renderPartial('login-form', [
                    'model' => $model,
                ]);

                return $response;
            }

            return $this->render('login', [
                'model' => $model,
            ]);

        }

        if (Yii::$app->request->isAjax) {
            $response[ 'html' ] = $this->renderAjax('login-modal', [
                'model' => $model,
            ]);

            return $response;
        }

        return $this->render('login', [
            'model' => $model,
        ]);

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionConfirmEmail($code)
    {
        $user = User::find()
                        ->where(['temporary_code'  => $code])
                        ->one();

        if (!$user) {
            throw new NotFoundHttpException('Не правильный код');
        }

        $user->confirm_email_at = new Expression('NOW()');
        $user->temporary_code   = NULL;

        $user->save(FALSE);

        return $this->render('success-confirm-email');
    }

    public function actionCheckUserAge($backUrl = NULL)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $cookies = Yii::$app->request->cookies;
        $response = [
            'form' => NULL,
            'html' => NULL,
            'reload' => FALSE,
            'redirect' => $backUrl,
        ];


        $model = new CheckUserAgeForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->checkUserAge()) {

                $response[ 'reload' ] = TRUE;
            } else {
                $response[ 'form' ] = $this->renderPartial('check-user-age-form', [
                    'model' => $model,
                ]);
            }

            return $response;
        }

        if (empty($response[ 'html' ])) {
            $response[ 'html' ] = $this->renderPartial('check-user-age', [
                'model' => $model,
            ]);
        }

        return $response;
    }

    public function actionSignup()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $cookies = Yii::$app->request->cookies;

        if (!$cookies->has('check-user-age')) {
            $model = new CheckUserAgeForm();
            $response[ 'html' ] = $this->renderPartial('check-user-age', [
                'model' => $model,
            ]);

            return $response;
        }

        $model = new SignupForm();
        $model->scenario = SignupForm::SCENARIO_SITE_REGISTER;

        $response = [
            'html'  => NULL,
            'form'  => NULL,
            'modal' => NULL,
        ];

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                //throw new \Exception("Error Processing Request", 1);

                $response[ 'modal' ] = $this->renderPartial('success-signup');
            } else {
                $response[ 'form' ] = $this->renderPartial('signup-form', [
                    'model' => $model,
                ]);
            }
        } else {
            $userDataBirth      = unserialize($cookies->get('check-user-age'));
            $model->dayBirth    = $userDataBirth[ 'dayBirth' ];
            $model->monthBirth  = $userDataBirth[ 'monthBirth' ];
            $model->yearBirth   = $userDataBirth[ 'yearBirth' ];

            $response[ 'html' ]  = $this->renderAjax('signup-modal', [
                'model' => $model,
                'action' => ['signup'],
            ]);
        }

        return $response;
    }

    public function actionSignupSocial()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $cookies = Yii::$app->request->cookies;
        $response = [
            'html'    => NULL,
            'form'    => NULL,
            'reload'  => FALSE,
        ];

        $model = new SignupForm();
        $model->load(Yii::$app->request->post());

        if ($user = $model->signupSocial()) {
            Yii::$app->user->login($user);
            $response[ 'reload' ] = TRUE;
        } else {
            $response[ 'form' ] = $this->renderPartial('signup-form', [
                'model' => $model,
                'action' => ['signup-social'],
            ]);
        }

        return $response;
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
