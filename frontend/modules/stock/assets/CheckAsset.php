<?php
namespace frontend\modules\stock\assets;

use yii\web\AssetBundle;

class CheckAsset extends AssetBundle
{

    public $sourcePath = '@app/modules/stock/assets/src';

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        $this->publishOptions[ 'forceCopy' ] = TRUE;
        $this->js[] = 'js/check.js';
    }
}
