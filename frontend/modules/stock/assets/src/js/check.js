$(function() {

    $(document).on('change', '.js-is-register', function( e ) {
        var $form = $(this).closest('form');

        if ($(this).prop('checked')) {
            $(document).find('#input-login-password').closest('.form-group').removeClass('hide');
            $(document).find('#btn-reset-password').removeClass('hide');
            $(document).find('#block-sms').addClass('hide');
            $(document).find('#block-register').addClass('hide');
        } else {
            $.ajax({
                url: $($form).data('url'),
                method: 'POST',
                data: $($form).serialize(),
                dataType: 'JSON',
            }).done(function(data) {
                //console.log(data);
                if (data.html) {
                    $($form).replaceWith(data.html);
                }
            });
        }
    });

    $(document).on('click', '#resend-sms', function( e ) {
        e.preventDefault();
        var $form = $(this).closest('form');

        $.ajax({
            url: $($form).data('url'),
            method: 'POST',
            data: $($form).serialize(),
            dataType: 'JSON',
        }).done(function(data) {
            //console.log(data);
            if (data.html) {
                $($form).replaceWith(data.html);
            }
        });
    });

});