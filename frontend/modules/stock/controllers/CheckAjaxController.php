<?php
namespace frontend\modules\stock\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\user\models\forms\CheckUserAgeForm;
use yii\web\Response;
use frontend\modules\stock\models\forms\RegisterCheckForm;
use frontend\modules\user\models\forms\LoginForm;
use frontend\modules\stock\models\Collection;
use frontend\modules\stock\models\forms\ChooseCollectionForm;
use frontend\modules\stock\models\Check;
use frontend\modules\stock\models\Prize;

class CheckAjaxController extends Controller
{

    public function actionCheckUserAge()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $cookies = Yii::$app->request->cookies;
        $response = [
            'html' => NULL,
            'form' => NULL,
            'reload' => NULL,
        ];

        if (!self::checkUserAge()) {
            return [
                'reload' => TRUE,
            ];
        }

        $model = new CheckUserAgeForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->checkUserAge()) {

                $response[ 'reload' ] = TRUE;
            } else {
                $response[ 'form' ] = $this->renderAjax('check-user-age-form', [
                    'model' => $model,
                ]);
            }

            return $response;
        }

        $response[ 'html' ] = $this->renderAjax('check-user-age', [
            'model' => $model,
        ]);

        return $response;
    }

    public function actionRegisterCheck()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $cookies = Yii::$app->request->cookies;
        $session = Yii::$app->session;
        $response = [
            'html' => NULL,
            'modal' => NULL,
            'form' => NULL,
        ];

        if (Yii::$app->user->isGuest) {
            $model = new LoginForm();
            $response[ 'html' ] = $this->renderAjax('@app/modules/user/views/security/login-modal', [
                'model' => $model,
            ]);

            return $response;
        }

        if (self::checkUserAge()) {
            $model = new CheckUserAgeForm();

            return [
                'html' => $this->renderAjax('check-user-age', [
                    'model' => $model,
                ]),
            ];
        }

        $model = new RegisterCheckForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->registerCheck()) {
                $response[ 'modal' ] = $this->renderPartial('success-register-check');
            } else {
                $response[ 'form' ] = $this->renderAjax('register-check-form', ['model' => $model]);

            }

            return $response;
        }

        $response[ 'html' ] = $this->renderAjax('register-check', ['model' => $model]);

        return $response;
    }

    public static function checkUserAge()
    {
        $cookies = Yii::$app->request->cookies;

        if (!$cookies->has('check-user-age')) {
            return TRUE;
        }

        return FALSE;
    }

    public function actionChooseListCollections($checkId)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [
            'html' => NULL,
            'form' => FALSE,
            'reload' => FALSE,
        ];

        $check = Check::find()
                            ->where(['id' => $checkId, 'user_id' => Yii::$app->user->identity->getId(), 'prize_id' => Prize::COLLECTION_PRIZE_ID])
                            ->andWhere(['is', 'collection_id', NULL])
                            ->one();

        $listCollections = Collection::getListChooseCollections();

        if (!$listCollections || !$check) {
            return $response;
        }

        $model = new ChooseCollectionForm();
        $model->setCollectionsList($listCollections);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->saveChooseUserCollection($check)) {
                $response[ 'reload' ] = TRUE;
            } else {
                $response[ 'form' ] = $this->renderPartial('modal-choose-collections-form', [
                    'model'   => $model,
                    'checkId' => $checkId,
                ]);
            }

            return $response;
        }

        $response[ 'html' ] = $this->renderPartial('modal-choose-collections', [
            'model'   => $model,
            'checkId' => $checkId,
        ]);

        return $response;
    }

}