<?php
namespace frontend\modules\stock\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\stock\models\Check;
use frontend\modules\stock\models\searchModels\CheckSearch;
use yii\grid\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use frontend\modules\stock\models\Prize;
use frontend\modules\stock\models\forms\RegisterCheckByRequest;
use yii\web\Response;

class CheckController extends Controller
{

    public function actionWinners()
    {
        $searchModel = new CheckSearch();
        $searchModel->load(Yii::$app->request->get());

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('winners', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listPrizes' => ArrayHelper::map(Prize::find()->where(['active' => 1])->orderBy(['display_order' => SORT_ASC])->all(), 'id', 'name'),
            'listDates' => ArrayHelper::map(Check::find()->where(['not', ['prize_id' => NULL]])->orderBy(['winner_date' => SORT_DESC])->all(), 'winner_date', function($model){ return Yii::$app->formatter->asDate($model->winner_date, 'php:d-m-Y');}),
        ]);
    }

    public function actionRegisterCheckByRequest()
    {
        $this->enableCsrfValidation = FALSE;

        Yii::$app->response->format = Response::FORMAT_RAW;

        $model        = new RegisterCheckByRequest();
        $model->txt   = Yii::$app->request->get('txt');
        $model->phone = preg_replace('/\D/', '', Yii::$app->request->get('phone'));
        $model->pref  = Yii::$app->request->get('pref');

        if ($model->registerCheck()) {
            return 'text=success';
        }

        return 'text=fail';
    }

}