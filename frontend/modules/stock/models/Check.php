<?php
namespace frontend\modules\stock\models;

use common\models\stock\Check as BaseCheck;
use frontend\modules\user\models\User;
use yii\helpers\Url;
use yii\helpers\Html;

class Check extends BaseCheck
{

    public function getPrizeStatus()
    {
        $user = $this->user;

        switch ($this->status) {
            case self::STATUS_REJECT:
                return 'Отклонен';
            case self::STATUS_ACCEPT:
                return ($user->address) ? 'Готово' : Html::a('Заполните адрес', ['/user/personal-area/index', 'tab' => 'delivery']);
        }
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getPrize()
    {
        return $this->hasOne(Prize::className(), ['id' => 'prize_id']);
    }

    public function getLinkSend()
    {
        return ($this->send_number) ? Html::a('https://www.pochta.ru/tracking#' . $this->send_number, 'https://www.pochta.ru/tracking#' . $this->send_number, ['target' => '_blank']) : NULL;
    }

    public function getSendStatus()
    {
        return $this->hasOne(SendStatus::className(), ['id' => 'send_status_id']);
    }

    public function getNamePrizes()
    {
        if (!$this->prize) {
            return NULL;
        }

        return $this->prize->name;
    }

    public function getDateWinners()
    {
        if ($this->winner_date) {
            return $this->winner_date;
        }

        return NULL;
    }

    public function getCheckStatus()
    {
        if ($this->prize_id) {
            if (!$this->user || !$this->user->profile) {
                return NULL;
            }

            if (!$this->user->profile->address) {
                return Html::a('Заполните адрес', ['/user/user/cabinet', 'tab' => 'delivery']);
            }
            if ($this->prize_id === Prize::COLLECTION_PRIZE_ID && !$this->collection_id) {
                return Html::a('Выберите мини-коллекциию', '#', ['class' => 'link-get-modal', 'data-url' => Url::toRoute(['/stock/check-ajax/choose-list-collections', 'checkId' => $this->id])]);
            }

            if ($this->status == self::STATUS_PROCESSING) {
                return 'Ожидает обработки';
            }
        }

        switch ($this->status) {
            //case self::STATUS_PROCESSING:
                //return 'Ожидает обработки';
            case self::STATUS_REJECT:
                return 'Отклонен';
            case self::STATUS_ACCEPT:
                return 'Готово';
            default:
                return 'Нет статуса';
        }
    }

    public function getHiddenPhone()
    {
        return substr($this->user_phone, 0, 1) . ' (' . substr($this->user_phone, 1, 3) . ')*****' . substr($this->user_phone, -2);
    }

}