<?php
namespace frontend\modules\stock\models;

use Yii;
use common\models\stock\Pet as BasePet;
use yii\helpers\ArrayHelper;

class Pet extends BasePet
{

    public static function getDropdownListPets()
    {
        return ArrayHelper::map(static::find()->where(['active' => 1])->orderBy(['display_order' => SORT_ASC])->all(), 'id', 'name');
    }

}