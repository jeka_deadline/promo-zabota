<?php
namespace frontend\modules\stock\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\User;
use yii\helpers\ArrayHelper;
use frontend\modules\stock\models\Check;
use yii\db\Expression;
use frontend\modules\stock\models\Store;
use yii\web\UploadedFile;

class RegisterCheckForm extends Model
{

    public $storeId;
    public $numberCheck;
    public $dateTimeCheck;
    public $imageCheck;
    public $captcha;
    public $agreeRule;

    public function init()
    {
        parent::init();
    }

    public function rules()
    {
        return [
            [['storeId', 'numberCheck', 'dateTimeCheck', 'agreeRule'], 'required'],
            [['agreeRule'], 'compare', 'compareValue' => 1],
            [['imageCheck'], 'file', 'extensions' => 'png,jpeg,jpg,bmp,pdf', 'skipOnEmpty' => FALSE],
            ['captcha', 'captcha', 'captchaAction' => '/core/index/captcha'],
            [['dateTimeCheck'], 'date', 'format' => 'php:d/m/Y H:i'],
            [['dateTimeCheck'], 'validateMysqlDatetime'],
        ];
    }

    public function registerCheck()
    {
        $this->imageCheck = UploadedFile::getInstance($this, 'imageCheck');

        if (!$this->validate()) {
            return FALSE;
        }

        $user = User::find()
                          ->where(['id' => Yii::$app->user->identity->getId()])
                          ->with('profile')
                          ->one();

        if (!$user || !$user->profile) {
            return FALSE;
        }

        $check                = new Check();
        $check->store_id      = $this->storeId;
        $check->number        = $this->numberCheck;
        $check->check_date    = Yii::$app->formatter->asDate(str_replace('/', '-', $this->dateTimeCheck), 'php: Y-m-d H:i:s');
        $check->user_id       = $user->id;
        $check->user_ip       = Yii::$app->request->userIp;
        $check->register_date = new Expression('NOW()');
        $check->user_phone    = $user->profile->phone;
        $check->user_name     = $user->profile->name;
        $check->user_email    = $user->email;
        $fileName             = $this->getHashFile($this->imageCheck->baseName);
        $directorySave        = Yii::getAlias('@frontend/web/') . Check::getFilePath();

        if (!file_exists($directorySave)) {
            mkdir($directorySave, 0777);
        }

        if ($this->imageCheck->saveAs($directorySave . DIRECTORY_SEPARATOR . $fileName . '.' . $this->imageCheck->extension)) {
            $check->check_image = $fileName . '.' . $this->imageCheck->extension;
            $check->image_hash = $fileName;
        }

        if ($check->validate() && $check->save()) {

            return TRUE;
        }

        return FALSE;

    }

    private function getHashFile($nameFile)
    {
        while (TRUE) {
            $hash = md5($nameFile + time());

            $check = Check::find()
                                ->where(['image_hash' => $hash])
                                ->one();

            if (!$check) {
                return $hash;
            }
        }
    }

    public function getStores()
    {
        return ArrayHelper::map(Store::find()->where(['active' => 1])->orderBy(['display_order' => SORT_ASC])->all(), 'id', function($model){ return $model->name; });
    }

    public function attributeLabels()
    {
        return [
            'storeId' => 'Торговая сеть, в которой совершена покупка',
            'numberCheck' => 'Номер чека',
            'dateTimeCheck' => 'Дата и время в чеке',
            'imageCheck' => 'Изображение чека',
            'captcha' => 'Введите символы с изображения',
        ];
    }

    public function validateMysqlDatetime($attribute, $params, $validator)
    {
        $startPointDate = strtotime('01-01-1970 00:00:00');
        $endPontDate    = strtotime(date('d-m-Y H:i:s'));
        $checkDate      = strtotime(str_replace('/', '-', $this->{$attribute}));

        if ($checkDate < $startPointDate || $checkDate > $endPontDate) {
            $message = (isset($params[ 'message' ])) ? $params[ 'message' ] : 'Не правильная дата';
            $this->addError($attribute, $message);
        }

        return TRUE;
    }
}