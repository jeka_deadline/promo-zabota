<?php
namespace frontend\modules\stock\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\user\models\Profile;
use frontend\modules\stock\models\Check;
use yii\db\Expression;

class RegisterCheckByRequest extends Model
{

    public $phone;
    public $txt;
    public $pref;

    public function rules()
    {
        return [
            [['phone', 'txt', 'pref'], 'required'],
            [['phone', 'txt', 'pref'], 'string'],
            [['phone'], 'filter', 'filter' => 'trim'],
        ];
    }

    public function registerCheck()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $phone = preg_replace('/\D/', '', $this->phone);

        $profile = Profile::find()
                                ->where(['phone' => $phone])
                                ->with('user')
                                ->one();

        $check                  = new Check();
        $check->number          = $this->txt;
        $check->sms_check_code  = mb_strtoupper($this->pref . ' ' . $this->txt, 'UTF-8');
        $check->register_date   = new Expression('NOW()');

        if ($profile && $profile->user) {
            $check->user_id     = $profile->user->id;
            $check->user_phone  = $phone;
            $check->user_email  = $profile->user->email;
            $check->user_name   = $profile->surname . ' ' . $profile->name;
        } else {
            $check->user_phone = $phone;

            $previousCheckSmsCode = Check::find()
                                              ->where(['user_phone' => $phone])
                                              ->andWhere(['not', ['attach_sms_code' => NULL]])
                                              ->one();

            if ($previousCheckSmsCode) {
                $check->attach_sms_code = $previousCheckSmsCode->attach_sms_code;
            } else {
                $check->attach_sms_code = $this->generateUniqueAttachSmsCode();

                $this->sendAttachSmsCode($check->attach_sms_code);
            }
        }

        if ($check->validate() && $check->save()) {
            return TRUE;
        }

        return FALSE;
    }

    public function sendAttachSmsCode()
    {

    }

    public function generateUniqueAttachSmsCode()
    {
        while (TRUE) {
            $number = '';

            for ($i = 0; $i < 5; $i++) {
                $number .= mt_rand(0, 9);
            }

            $check = Check::find()
                                ->where(['attach_sms_code' => $number])
                                ->one();

            if (!$check) {
                return $number;
            }
        }
    }

}