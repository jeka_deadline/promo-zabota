<?php
namespace frontend\modules\stock\models\forms;

use yii\base\Model;
use frontend\modules\stock\models\Collection;

class ChooseCollectionForm extends Model
{

    public $collectionId;

    private $_collectionsList;

    public function rules()
    {
        return [
            ['collectionId', 'required'],
            ['collectionId', 'exist', 'targetClass' => Collection::className(), 'targetAttribute' => 'id', 'filter' => function($query) {
                return $query->andWhere(['active' => 1]);
            }],
        ];
    }

    public function saveChooseUserCollection($check)
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $check->collection_id = $this->collectionId;

        $check->save(FALSE);

        return TRUE;
    }

    public function setCollectionsList($listCollections)
    {
        foreach ($listCollections as $collection) {
            $this->_collectionsList[ $collection->id ] = $collection->name;
        }
    }

    public function getListCollections()
    {
        return $this->_collectionsList;
    }

}