<?php
namespace frontend\modules\stock\models;

use Yii;
use common\models\stock\Collection as BaseCollection;
use yii\helpers\ArrayHelper;

class Collection extends BaseCollection
{

    public static function getListChooseCollections()
    {
        $listCollections = ArrayHelper::map(self::find()->where(['active' => 1])->all(), 'id', function($model){ return $model; });

        foreach ($listCollections as $id => $collection) {
            $countChecks = Check::find()->where(['collection_id' => $id])->count();

            if ($countChecks >= $collection->count) {
                unset($listCollections[ $id ]);
            }
        }

        return $listCollections;
    }

}