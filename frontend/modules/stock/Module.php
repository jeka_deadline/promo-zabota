<?php
namespace frontend\modules\stock;

use yii\base\Module as BaseModule;

class Module extends BaseModule
{

    public $controllerNamespace = 'frontend\modules\stock\controllers';

    public function init()
    {
        return parent::init();
    }

}