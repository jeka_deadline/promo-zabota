<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'ajax-form'
    ],
    'action' => ['/stock/check-ajax/check-user-age'],
]); ?>

    <?= $form->field($model, 'dayBirth'); ?>

    <?= $form->field($model, 'monthBirth')->dropDownList($model->getListMonths()); ?>

    <?= $form->field($model, 'yearBirth'); ?>

    <?= Html::submitButton('Продолжить', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>