<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
    'action' => ['/stock/check-ajax/choose-list-collections', 'checkId' => $checkId],
    'options' => [
        'class' => 'ajax-form',
    ],
]); ?>

    <?= $form->field($model, 'collectionId')->radioList($model->getListCollections()); ?>

    <?= Html::submitButton('Далее', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>