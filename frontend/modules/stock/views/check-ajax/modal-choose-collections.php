<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin([
    'header' => 'Сообщение'
]); ?>

    <p>Выберите приз</p>

    <?= $this->render('modal-choose-collections-form', [
        'model'   => $model,
        'checkId' => $checkId,
    ]); ?>

<?php Modal::end(); ?>