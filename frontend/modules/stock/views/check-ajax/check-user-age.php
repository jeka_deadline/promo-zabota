<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin(); ?>

    <?= $this->render('check-user-age-form', [
        'model' => $model,
    ]); ?>

<?php Modal::end(); ?>