<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
?>

<?php $form = ActiveForm::begin([
    'action' => ['/stock/check-ajax/register-check'],
    'options' => [
        'id' => 'register-form-step2',
        'enctype'=>'multipart/form-data',
    ],
]); ?>

    <?= $form->field($model, 'storeId')->radioList($model->getStores()); ?>

    <?= $form->field($model, 'numberCheck'); ?>

    <?= $form->field($model, 'dateTimeCheck')->widget(MaskedInput::className(), [
        'clientOptions' => [
            'alias' =>  'datetime',
        ],
    ]); ?>

    <?= $form->field($model, 'imageCheck')->fileInput(); ?>

    <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::classname(), [
        'captchaAction' => '/core/index/captcha',
    ]) ?>

    <?= $form->field($model, 'agreeRule')->checkbox(); ?>

    <?= Html::submitButton('Далее', ['class' => 'btn btn-success']); ?>

<?php ActiveForm::end(); ?>