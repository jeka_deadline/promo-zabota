<?php
use yii\bootstrap\Modal;
?>

<?php Modal::begin([
    'header' => 'Регистрация чека',
]); ?>

    <h2>Регистрация чека</h1>
    <h4>Заполните пожалуйста поля ниже</h4>

    <?= $this->render('register-check-form', [
        'model' => $model,
    ]); ?>

<?php Modal::end(); ?>