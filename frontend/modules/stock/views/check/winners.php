<?php
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
    'action' => ['winners'],
    'method' => 'GET',
]); ?>

    <?= $form->field($searchModel, 'userNameOrPhone'); ?>

    <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'attribute' => 'userNameOrPhone',
            'label' => 'Имя',
            'value' => function($model){ return $model->user_name;},
            'filter' => FALSE,
        ],
        [
            'attribute' => 'userNameOrPhone',
            'label' => 'Номер телефона',
            'value' => function($model){ return $model->getHiddenPhone(); },
            'filter' => FALSE,
        ],
        [
            'attribute' => 'prize_id',
            'label' => 'Приз',
            'value' => function($model){ return $model->prize->name;},
            'filter' => $listPrizes,
        ],
        [
            'attribute' => 'winner_date',
            'filter' => $listDates,
            'label' => 'Дата',
            'value' => function($model){ return Yii::$app->formatter->asDate($model->winner_date, 'php:d-m-Y');},
        ],
    ],
]); ?>