<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\modules\core\models\SupportSubject;
?>

<?php $form = ActiveForm::begin([
    'options' => ['class' => 'ajax-form'],
]); ?>

    <?php if (Yii::$app->user->isGuest) : ?>

        <?= $form->field($model, 'name'); ?>

        <?= $form->field($model, 'email'); ?>

    <?php endif; ?>

    <?= $form->field($model, 'subjectId')->dropDownList($model->getListSupportSubjects(), ['prompt' => 'Выберите тему сообщения', 'id' => 'js-select-subject-support']); ?>

    <?= $form->field($model, 'customSubject', [
        'options' => [
            'id'    => 'wrapper-custom-subject',
            'class' => ($model->subjectId == SupportSubject::SUPPORT_CUSTOM_SUBJECT_ID) ? '' : 'hide',
        ]
    ])->textInput(); ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 10]); ?>

    <?= $form->field($model, 'image')->fileInput(); ?>

    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>