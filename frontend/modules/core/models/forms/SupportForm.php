<?php
namespace frontend\modules\core\models\forms;

use Yii;
use yii\base\Model;
use frontend\modules\core\models\SupportSubject;
use frontend\modules\core\models\SupportMessage;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

class SupportForm extends Model
{

    public $name;
    public $email;
    public $subjectId;
    public $text;
    public $customSubject;
    public $image;

    const SCENARIO_GUEST = 'guest';

    public function rules()
    {
        return [
            [['subjectId', 'text'], 'required'],
            [['name', 'email'], 'required', 'on' => self::SCENARIO_GUEST],
            [['name', 'email'], 'string', 'max' => 100, 'on' => self::SCENARIO_GUEST],
            [['email'], 'email', 'on' => self::SCENARIO_GUEST],
            [['text'], 'string'],
            [['subjectId'], 'exist', 'targetClass' => SupportSubject::className(), 'targetAttribute' => 'id', 'filter' => function($query){
                  return $query->andWhere(['active' => 1]);
            }],
            [['customSubject'], 'required', 'when' => function($model) {
                return $this->subjectId == SupportSubject::SUPPORT_CUSTOM_SUBJECT_ID;
            }],
            ['customSubject', 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'bmp, jpeg, jpg, png', 'skipOnEmpty' => TRUE],
        ];
    }

    public function getListSupportSubjects()
    {
        return ArrayHelper::map(SupportSubject::find()->where(['active' => 1])->orderBy(['display_order' => SORT_ASC])->all(), 'id', 'name');
    }

    public function createSupportMessage()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $model = new SupportMessage();
        $model->subject_id = $this->subjectId;

        if (Yii::$app->user->isGuest) {
            $model->name = $this->name;
            $model->email = $this->email;
        } else {
            $model->name = Yii::$app->user->identity->profile->surname . ' ' . Yii::$app->user->identity->profile->name;
            $model->email = Yii::$app->user->identity->email;
            $model->user_id = Yii::$app->user->identity->id;
        }

        $model->text            = $this->text;

        if ($this->subjectId == SupportSubject::SUPPORT_CUSTOM_SUBJECT_ID) {
            $model->custom_subject  = $this->customSubject;
        }

        $file = UploadedFile::getInstance($this, 'image');

        if ($file) {
            $fileName = md5($file->baseName);
            $uploadPath = Yii::getAlias('@frontend/web') . DIRECTORY_SEPARATOR . SupportMessage::getFilePath();

            if (!is_file($uploadPath) || !is_dir($uploadPath)) {
                mkdir($uploadPath, 0777);
            }

            $file->saveAs($uploadPath. DIRECTORY_SEPARATOR . $fileName . '.' . $file->extension);
            $model->image = $fileName . '.' . $file->extension;
        }

        if ($model->validate() && $model->save()) {
            return TRUE;
        }

        return FALSE;
    }

}