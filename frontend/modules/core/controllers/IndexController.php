<?php
namespace frontend\modules\core\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\modules\core\models\ContactForm;
use frontend\modules\user\models\forms\SignupForm;
use yii\helpers\Html;

class IndexController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $session = Yii::$app->session;

        if ($session->has('social-user-attributes')) {

            $hash   = $session->get('social-user-attributes');
            $model  = new SignupForm();

            $model->setAttributes(unserialize(base64_decode($hash)));
            $session->remove('social-user-attributes');

            $modal = $this->renderAjax('@app/modules/user/views/security/signup-modal', [
                'model' => $model,
                'action' => ['/user/security/signup-social'],
            ]);

            $jsonModal = json_encode(['modal' => $modal]);

            $script = <<< JS
                var jsonModal = {$jsonModal};
                console.log(jsonModal.modal);
                $(document).find('#modals').empty().append(jsonModal.modal);
                $('.modal').modal();
JS;

            $this->view->registerJs($script);
        }
        return $this->render('index');
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionRules()
    {
        return $this->render('rules');
    }

}
