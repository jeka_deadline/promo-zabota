<?php
namespace frontend\modules\core\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\core\models\forms\SupportForm;
use yii\web\Response;

class SupportAjaxController extends Controller
{

    public function actionSupportMessage()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = [
            'html' => NULL,
            'form' => NULL,
        ];
        $model = new SupportForm();

        if (Yii::$app->user->isGuest) {
            $model->scenario = SupportForm::SCENARIO_GUEST;
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->createSupportMessage()) {
                $response[ 'form' ] = $this->renderPartial('success-create-support-message');
            } else {
                $response[ 'form' ] = $this->renderPartial('support-form', [
                    'model' => $model,
                ]);
            }
        } else {
            $response[ 'html' ] = $this->renderPartial('support-modal', [
                'model' => $model,
            ]);
        }

        return $response;
    }

}