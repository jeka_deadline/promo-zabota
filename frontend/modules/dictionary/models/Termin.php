<?php
namespace frontend\modules\dictionary\models;

use Yii;
use common\models\dictionary\Termin as BaseTermin;
use yii\helpers\ArrayHelper;

class Termin extends BaseTermin
{

    public static function generateListAcardion()
    {
        $list = [];

        $termins = static::find()->where(['active' => 1])->orderBy(['display_order' => SORT_ASC])->all();

        foreach ($termins as $termin) {
            $list[] = [
                'header' => $termin->name,
                'content' => $termin->text,
            ];
        }

        return $list;
    }

}