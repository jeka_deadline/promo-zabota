<?php
namespace frontend\modules\dictionary\controllers;

use Yii;
use yii\web\Controller;
use frontend\modules\dictionary\models\Termin;

class FaqController extends Controller
{

    public function actionIndex()
    {
        return $this->render('faq', [
            'listQuestions' => Termin::generateListAcardion(),
        ]);
    }

}