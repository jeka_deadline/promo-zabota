<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\Accordion;
?>

<?= Accordion::widget([
    'items' => $listQuestions,
    'options' => ['tag' => 'div'],
    'itemOptions' => ['tag' => 'div'],
    'headerOptions' => ['tag' => 'h3'],
    'clientOptions' => ['collapsible' => false],
]); ?>

Если Вы не увидели своего вопроса, <a href="#" class="link-get-modal" data-url="<?= Url::toRoute(['/core/support-ajax/support-message']); ?>">свяжитесь с нами</a>