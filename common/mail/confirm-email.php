<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
Для подтверждения данного email перейдите по этой <?= Html::a('ссылке', Url::toRoute(['/user/security/confirm-email', 'code' => $code])); ?> или же скопируйте эту ссылку - <?= Url::toRoute(['/user/security/confirm-email', 'code' => $code]); ?>