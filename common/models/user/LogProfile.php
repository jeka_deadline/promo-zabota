<?php

namespace common\models\user;

use Yii;

/**
 * This is the model class for table "{{%user_log_profiles}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $surname
 * @property string $name
 * @property string $date_birth
 * @property string $phone
 * @property string $city
 * @property string $region
 * @property string $address
 * @property string $code_change_phone
 *
 * @property User $user
 */
class LogProfile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_log_profiles}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['date_birth'], 'safe'],
            [['surname', 'name', 'city', 'region'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 15],
            [['address'], 'string', 'max' => 255],
            [['code_change_phone'], 'string', 'max' => 20],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'surname' => 'Surname',
            'name' => 'Name',
            'date_birth' => 'Date Birth',
            'phone' => 'Phone',
            'city' => 'City',
            'region' => 'Region',
            'address' => 'Address',
            'code_change_phone' => 'Code Change Phone',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
