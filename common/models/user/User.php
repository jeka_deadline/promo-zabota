<?php

namespace common\models\user;

use Yii;

/**
 * This is the model class for table "{{%user_users}}".
 *
 * @property integer $id
 * @property integer $is_admin
 * @property string $email
 * @property string $temporary_code
 * @property string $password_hash
 * @property string $reset_password_token
 * @property string $auth_key
 * @property string $blocked_at
 * @property string $confirm_email_at
 * @property string $confirm_phone_at
 * @property string $register_ip
 * @property string $created_at
 * @property string $updated_at
 * @property integer $login_with_social
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_admin', 'login_with_social'], 'integer'],
            [['email', 'password_hash', 'auth_key', 'register_ip'], 'required'],
            [['blocked_at', 'confirm_email_at', 'confirm_phone_at', 'created_at', 'updated_at'], 'safe'],
            [['email', 'password_hash'], 'string', 'max' => 100],
            [['temporary_code', 'reset_password_token', 'auth_key'], 'string', 'max' => 32],
            [['register_ip'], 'string', 'max' => 15],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_admin' => 'Is Admin',
            'email' => 'Email',
            'temporary_code' => 'Temporary Code',
            'password_hash' => 'Password Hash',
            'reset_password_token' => 'Reset Password Token',
            'auth_key' => 'Auth Key',
            'blocked_at' => 'Blocked At',
            'confirm_email_at' => 'Confirm Email At',
            'confirm_phone_at' => 'Confirm Phone At',
            'register_ip' => 'Register Ip',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'login_with_social' => 'Login With Social',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setRegisterIp()
    {
        $this->register_ip = Yii::$app->request->userIP;
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasMany(Profile::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function geSocial()
    {
        return $this->hasMany(Social::className(), ['user_id' => 'id']);
    }
}
