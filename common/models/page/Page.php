<?php

namespace common\models\page;

use Yii;

/**
 * This is the model class for table "{{%page_static_pages}}".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $uri
 * @property string $full_uri
 * @property string $header
 * @property string $menu_class
 * @property string $content
 * @property integer $display_order
 * @property integer $active
 */
class Page extends \yii\db\ActiveRecord
{
    private static $filePath = 'files/page';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_static_pages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'display_order', 'active'], 'integer'],
            [['uri', 'header'], 'required'],
            [['content'], 'string'],
            [['uri'], 'string', 'max' => 100],
            [['full_uri', 'header', 'menu_class'], 'string', 'max' => 255],
            [['uri'], 'unique'],
            [['full_uri'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'uri' => 'Uri',
            'full_uri' => 'Full Uri',
            'header' => 'Header',
            'menu_class' => 'Menu Class',
            'content' => 'Content',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    public static function getFilePath()
    {
        return self::$filePath;
    }
}
