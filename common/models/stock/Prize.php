<?php

namespace common\models\stock;

use Yii;

/**
 * This is the model class for table "{{%stock_prizes}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property integer $display_order
 * @property integer $active
 */
class Prize extends \yii\db\ActiveRecord
{

    const COLLECTION_PRIZE_ID   = 1;
    const CERTIFICATE_PRIZE_ID  = 2;

    public static $filePath = 'files/stock/prizes';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stock_prizes}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['display_order', 'active'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image' => 'Image',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    public static function getFilePath()
    {
        return self::$filePath;
    }
}
