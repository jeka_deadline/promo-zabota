<?php

namespace common\models\stock;

use Yii;

/**
 * This is the model class for table "{{%stock_pets}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $display_order
 * @property integer $active
 *
 * @property StockCheck[] $stockChecks
 */
class Pet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stock_pets}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['display_order', 'active'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockChecks()
    {
        return $this->hasMany(StockCheck::className(), ['pet_id' => 'id']);
    }
}
