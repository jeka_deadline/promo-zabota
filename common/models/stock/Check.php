<?php

namespace common\models\stock;

use Yii;
use common\models\user\User;

/**
 * This is the model class for table "{{%stock_check}}".
 *
 * @property integer $id
 * @property string $hash_id
 * @property integer $store_id
 * @property integer $user_id
 * @property string $user_email
 * @property string $user_name
 * @property string $user_phone
 * @property string $sms_check_code
 * @property string $number
 * @property string $check_date
 * @property string $check_image
 * @property string $image_hash
 * @property string $register_date
 * @property integer $status
 * @property string $reason_rejection
 * @property integer $prize_id
 * @property string $winner_date
 * @property integer $send_status_id
 * @property string $send_number
 * @property string $user_ip
 *
 * @property Prize $prize
 * @property SendStatus $sendStatus
 * @property Store $store
 * @property User $user
 */
class Check extends \yii\db\ActiveRecord
{
    const STATUS_PROCESSING = 0;
    const STATUS_REJECT = 1;
    const STATUS_ACCEPT = 2;

    const COLLECTION_PRIZE_ID = 1;
    const CERTIFICATE_PRIZE_ID = 2;

    public static $filePath = 'files/checks';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stock_check}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_id', 'user_id', 'status', 'prize_id', 'send_status_id'], 'integer'],
            [['number'], 'required'],
            [['check_date', 'register_date', 'winner_date'], 'safe'],
            [['check_image', 'reason_rejection'], 'string'],
            [['hash_id', 'image_hash'], 'string', 'max' => 32],
            [['user_email', 'user_name'], 'string', 'max' => 100],
            [['user_phone', 'user_ip'], 'string', 'max' => 15],
            [['sms_check_code'], 'string', 'max' => 255],
            [['number', 'send_number'], 'string', 'max' => 50],
            [['prize_id'], 'exist', 'skipOnError' => true, 'targetClass' => Prize::className(), 'targetAttribute' => ['prize_id' => 'id']],
            [['send_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => SendStatus::className(), 'targetAttribute' => ['send_status_id' => 'id']],
            [['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['store_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hash_id' => 'Hash ID',
            'store_id' => 'Store ID',
            'user_id' => 'User ID',
            'user_email' => 'User Email',
            'user_name' => 'User Name',
            'user_phone' => 'User Phone',
            'sms_check_code' => 'Sms Check Code',
            'number' => 'Number',
            'check_date' => 'Check Date',
            'check_image' => 'Check Image',
            'image_hash' => 'Image Hash',
            'register_date' => 'Register Date',
            'status' => 'Status',
            'reason_rejection' => 'Reason Rejection',
            'prize_id' => 'Prize ID',
            'winner_date' => 'Winner Date',
            'send_status_id' => 'Send Status ID',
            'send_number' => 'Send Number',
            'user_ip' => 'User Ip',
        ];
    }

    public static function getFilePath()
    {
        return self::$filePath;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrize()
    {
        return $this->hasOne(Prize::className(), ['id' => 'prize_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSendStatus()
    {
        return $this->hasOne(SendStatus::className(), ['id' => 'send_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::className(), ['id' => 'store_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
