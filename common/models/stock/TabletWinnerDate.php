<?php

namespace common\models\stock;

use Yii;

/**
 * This is the model class for table "{{%stock_tablet_winner_date}}".
 *
 * @property integer $id
 * @property string $date
 * @property integer $is_winner
 */
class TabletWinnerDate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stock_tablet_winner_date}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'required'],
            [['date'], 'safe'],
            [['is_winner'], 'integer'],
            [['date'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'is_winner' => 'Is Winner',
        ];
    }
}
