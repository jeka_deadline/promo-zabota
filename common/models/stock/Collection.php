<?php

namespace common\models\stock;

use Yii;

/**
 * This is the model class for table "{{%stock_collections}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property integer $count
 * @property integer $display_order
 * @property integer $active
 */
class Collection extends \yii\db\ActiveRecord
{
    public static $filePath = 'files/Collection/prizes';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stock_collections}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['count', 'display_order', 'active'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'image' => 'Image',
            'count' => 'Count',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }

    public static function getFilePath()
    {
        return self::$filePath;
    }
}
