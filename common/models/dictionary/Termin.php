<?php

namespace common\models\dictionary;

use Yii;

/**
 * This is the model class for table "{{%dictionary_termins}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property integer $display_order
 * @property integer $active
 */
class Termin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dictionary_termins}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'text'], 'required'],
            [['text'], 'string'],
            [['display_order', 'active'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'text' => 'Text',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }
}
