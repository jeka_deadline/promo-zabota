<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_params}}".
 *
 * @property integer $id
 * @property integer $type_id
 * @property integer $group_id
 * @property string $name
 * @property string $code
 * @property string $description
 * @property string $value
 */
class Param extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_params}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'group_id', 'name'], 'required'],
            [['type_id', 'group_id'], 'integer'],
            [['description', 'value'], 'string'],
            [['name', 'code'], 'string', 'max' => 255],
            [['code'], 'unique'],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => GroupParam::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TypeFieldForm::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Type ID',
            'group_id' => 'Group ID',
            'name' => 'Name',
            'code' => 'Code',
            'description' => 'Description',
            'value' => 'Value',
        ];
    }
}
