<?php

namespace common\models\core;

use Yii;

/**
 * This is the model class for table "{{%core_support_subjects}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $display_order
 * @property integer $active
 */
class SupportSubject extends \yii\db\ActiveRecord
{

    const SUPPORT_CUSTOM_SUBJECT_ID = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_support_subjects}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['display_order', 'active'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'display_order' => 'Display Order',
            'active' => 'Active',
        ];
    }
}
