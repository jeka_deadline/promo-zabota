<?php

namespace common\models\core;

use Yii;
use common\models\user\User;

/**
 * This is the model class for table "{{%core_support_messages}}".
 *
 * @property integer $id
 * @property integer $subject_id
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property string $custom_subject
 * @property string $text
 * @property string $answer
 * @property string $image
 * @property integer $is_new
 * @property string $date
 *
 * @property CoreSupportSubjects $subject
 * @property UserUsers $user
 */
class SupportMessage extends \yii\db\ActiveRecord
{

    public static $filePath = 'files/core/support';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%core_support_messages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_id', 'name', 'email', 'text'], 'required'],
            [['subject_id', 'user_id', 'is_new'], 'integer'],
            [['text', 'answer'], 'string'],
            [['date'], 'safe'],
            [['name', 'email'], 'string', 'max' => 100],
            [['custom_subject', 'image'], 'string', 'max' => 255],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => SupportSubject::className(), 'targetAttribute' => ['subject_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject_id' => 'Subject ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'email' => 'Email',
            'custom_subject' => 'Custom Subject',
            'text' => 'Text',
            'answer' => 'Answer',
            'image' => 'Image',
            'is_new' => 'Is New',
            'date' => 'Date',
        ];
    }

    public static function getFilePath()
    {
        return self::$filePath;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(SupportSubject::className(), ['id' => 'subject_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
