<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\stock\Check;
use DateTime;
use yii\helpers\ArrayHelper;

class WinnerController extends Controller
{

    // дата начала учета чеков при разыгрывании сертификатов первого этапа
    private $_startDateCertificateFirstStep = '2017-09-01 10:00:00';

    // дата начала учета чеков при разыгрывании сертификатов второго этапа
    private $_startDateCertificateSecondStep = '2017-09-15 23:59:59';

    // дата конца учета чеков при разыгрывании сертификатов первого этапа
    private $_endDateCertificateFirstStep = '2017-09-16 00:00:00';

    // дата конца учета чеков при разыгрывании сертификатов второго этапа
    private $_endDateCertificateSecondStep = '2017-09-30 23:59:59';

    // дата разыгрывании сертификатов первого этапа
    private $_dateFirstStep = '2017-09-18';

    // дата разыгрывании сертификатов второго этапа
    private $_dateSecondStep = '2017-10-02';

    public function actionGetCollectionWinner()
    {
        Yii::info(str_repeat('=', 50), 'winner');
        $currentDate  = date('Y-m-d');
        $date         = new DateTime($currentDate);
        $weekend      = date('N');

        if ($weekend == 6 || $weekend == 7) {
            Yii::info(date('d-m-Y H:i:s') . " Розыграш призов в субботу и воскресенье не проводится", 'winner');
            return FALSE;
        }

        if ($weekend == 1) {
            $date->modify('-3 day');
        } else {
            $date->modify('-1 day');
        }

        $winnerDate = $date->format('Y-m-d');

        Yii::info(date('d-m-Y H:i:s') . " Начинаем розыгрыш приза миниатюры за дату = {$currentDate}", 'winner');

        $check = Check::find()
                            ->where(['DATE(winner_date)' => $currentDate])
                            ->andWhere(['prize_id' => COLLECTION_PRIZE_ID])
                            ->one();

        if ($check) {
            Yii::info("Розыгрыш приза миниатюры за дату = {$currentDate} закончен", 'winner');
            return FALSE;
        }

        $query = Check::find()
                          ->andWhere(['between', 'register_date', $winnerDate, $currentDate])
                          ->orderBy(['register_date' => SORT_ASC]);

        $countChecks  = $query->count();

        Yii::info("Получаем список призов и идем по нему циклом", 'winner');
        $hashId = md5(time() . Yii::$app->security->generateRandomString());

        Yii::info("Разыгрываем приз с HASH ID {$hashId}", 'winner');
        Yii::info("Чеков зарегистрировано: {$countChecks}", 'winner');

        if (!$countChecks) {
            return FALSE;
        }
        $checks = $query->all();

        $numbersChecks = array_keys(ArrayHelper::map($checks, 'id', 'id'));

        shuffle($checks);

        while ($currentCheck = array_pop($checks)) {
            if ($currentCheck->prize_id) {
                Yii::info("Чек с id {$currentCheck->id} уже побеждал за текущий день", 'winner');
                continue;
            }

            if ($currentCheck->user) {
                $countWinnersChecksByUser = Check::find()
                                                      ->where(['user_id' => $currentCheck->user->id])
                                                      ->andWhere(['not', ['prize_id' => NULL]])
                                                      ->count();
            } else {
                $countWinnersChecksByUser = Check::find()
                                                      ->where(['user_phone' => $currentCheck->user_phone])
                                                      ->andWhere(['not', ['prize_id' => NULL]])
                                                      ->count();
            }

            if ($countWinnersChecksByUser > 4) {
                Yii::info("Юзер c идентификатором системы {$currentCheck->user_phone} уже получал приз 4 раза", 'winner');
                continue;
            }

            $currentCheck->winner_date = $currentDate;
            $currentCheck->prize_id    = Check::COLLECTION_PRIZE_ID;
            $currentCheck->hash_id     = $hashId;

            $currentCheck->save(FALSE);

            $winner = array_search($currentCheck->id, $numbersChecks);

            Yii::info("Победил чек с номером: {$winner} с ID {$currentCheck->id}", 'winner');

            return FALSE;
        }
    }

    public function actionGetCertificateWinner()
    {
        Yii::info(str_repeat('=', 50), 'winner');
        $currentDate = date('Y-m-d');

        $datesWinnersCertificates = [
            $this->_dateFirstStep, $this->_dateSecondStep
        ];

        if (array_search($currentDate, $datesWinnersCertificates) === FALSE) {
            Yii::info(date('d-m-Y H:i:s') . " Розыгрыш сертификата за дату = {$currentDate} невозможен", 'winner');
            return FALSE;
        }

        Yii::info(date('d-m-Y H:i:s') . " Начинаем розыгрыш приза сертификата за дату = {$currentDate}", 'winner');

        $check = Check::find()
                            ->where(['DATE(winner_date)' => $currentDate])
                            ->andWhere(['prize_id' => Check::CERTIFICATE_PRIZE_ID])
                            ->one();

        if ($check) {
            Yii::info("Розыгрыш приза сертификата за дату = {$currentDate} закончен", 'winner');
            return FALSE;
        }

        if ($currentDate == $this->_dateFirstStep) {
            $startDate = $this->_startDateCertificateFirstStep;
            $endDate = $this->_endDateCertificateFirstStep;
        } else {
            $startDate = $this->_startDateCertificateSecondStep;
            $endDate = $this->_endDateCertificateSecondStep;
        }

        $query = Check::find()
                            ->orderBy(['register_date' => SORT_ASC])
                            ->andWhere(['between', 'register_date', $startDate, $endDate]);

        $countChecks  = $query->count();

        Yii::info("Получаем список призов и идем по нему циклом", 'winner');
        $hashId = md5(time() . Yii::$app->security->generateRandomString());

        Yii::info("Разыгрываем приз с HASH ID {$hashId}", 'winner');
        Yii::info("Чеков зарегистрировано: {$countChecks}", 'winner');

        if (!$countChecks) {
            return FALSE;
        }

        $checks = $query->all();

        $numbersChecks = array_keys(ArrayHelper::map($checks, 'id', 'id'));

        shuffle($checks);

        while ($currentCheck = array_pop($checks)) {
            if ($currentCheck->prize_id) {
                Yii::info("Чек с id {$currentCheck->id} уже выигрывал приз", 'winner');
                continue;
            }

            $currentCheck->winner_date  = $currentDate;
            $currentCheck->prize_id     = Check::CERTIFICATE_PRIZE_ID;
            $currentCheck->hash_id      = $hashId;

            $currentCheck->save(FALSE);

            $winner = array_search($currentCheck->id, $numbersChecks);

            Yii::info("Победил чек с номером: {$winner} с ID {$currentCheck->id}", 'winner');

            return FALSE;

        }
    }

}
?>