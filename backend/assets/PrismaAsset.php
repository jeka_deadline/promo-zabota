<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class PrismaAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/themes/prisma/';
    //public $sourcePath = '@app/themes/prisma/assets';

    public $css = [
        'css/libs/font-awesome.css',
        'css/libs/nanoscroller.css',
        'css/compiled/theme_styles.css',
        'css/libs/daterangepicker.css',
        'css/libs/jquery-jvectormap-1.2.2.css',
        'css/libs/weather-icons.css',
        'css/libs/morris.css',
        '//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300',
    ];
    public $js = [
        'js/demo-skin-changer.js',
        'js/bootstrap.js',
        'js/jquery.nanoscroller.min.js',
        'js/jquery.scrollTo.min.js',
        'js/demo.js',
        'js/jquery.slimscroll.min.js',
        'js/moment.min.js',
        'js/jquery-jvectormap-1.2.2.min.js',
        'js/jquery-jvectormap-world-merc-en.js',
        'js/gdp-data.js',
        'js/flot/jquery.flot.min.js',
        'js/flot/jquery.flot.resize.min.js',
        'js/flot/jquery.flot.time.min.js',
        'js/flot/jquery.flot.threshold.js',
        'js/flot/jquery.flot.axislabels.js',
        'js/jquery.sparkline.min.js',
        'js/skycons.js',
        'js/raphael-min.js',
        'js/scripts.js',
        'js/pace.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init()
    {
        parent::init();
        $this->publishOptions[ 'forceCopy' ] = TRUE;
    }
}
