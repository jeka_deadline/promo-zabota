<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class JsTreeAsset extends AssetBundle
{
    //public $sourcePath = '@app/themes/default/assets';

    public $css = [
        'https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css',
    ];
    public $js = [
        'https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}
