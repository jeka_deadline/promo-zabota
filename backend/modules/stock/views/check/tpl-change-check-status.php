<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
    'action' => ['/raffle/check/change-check-status', 'id' => $model->id],
]); ?>

    <?= $form->field($model, 'status')->dropDownList($listStatuses, ['prompt' => '']); ?>

    <?= $form->field($model, 'reason_rejection')->textarea(['rows' => 10]); ?>

    <?= Html::submitButton(Yii::t('core', 'Save'), ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>