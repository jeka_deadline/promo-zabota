<?php
namespace backend\modules\stock\models;

use Yii;
use common\models\stock\TabletWinnerDate as BaseTabletWinnerDate;
use yii\helpers\ArrayHelper;
use trntv\yii\datetime\DateTimeWidget;

class TabletWinnerDate extends BaseTabletWinnerDate
{

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
              [['is_winner'], 'default', 'value' => 0],
          ]);
    }

    public function getFormElements()
    {
        $fields =  [
            'date' => ['type' => 'widget', 'nameWidget' => DateTimeWidget::className(), 'attributes' => ['phpDatetimeFormat' => 'yyyy-MM-dd']],
        ];

        return $fields;
    }

    public function attributeLabels()
    {
        return [
            'is_winner' => 'Проводился ли розыгрыш?',
            'date' => Yii::t('core', 'Date'),
        ];
    }
}