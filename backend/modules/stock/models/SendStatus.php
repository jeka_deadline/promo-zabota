<?php
namespace backend\modules\stock\models;

use Yii;
use common\models\stock\SendStatus as BaseSendStatus;

class SendStatus extends BaseSendStatus
{

    public function getFormElements()
    {
        $fields =  [
            'name' => ['type' => 'text'],
        ];

        return $fields;
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название',
        ];
    }

}