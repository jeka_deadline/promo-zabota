<?php

namespace backend\modules\stock\models\searchModels;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\stock\models\Check;

/**
 * CheckSearch represents the model behind the search form about `backend\modules\stock\models\Check`.
 */
class CheckSearch extends Check
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'store_id', 'user_id', 'status', 'prize_id', 'send_status_id'], 'integer'],
            [['hash_id', 'user_email', 'user_name', 'user_phone', 'sms_check_code', 'number', 'check_date', 'check_image', 'image_hash', 'register_date', 'reason_rejection', 'winner_date', 'send_number', 'user_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Check::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'store_id' => $this->store_id,
            'user_id' => $this->user_id,
            'check_date' => $this->check_date,
            'register_date' => $this->register_date,
            'status' => $this->status,
            'prize_id' => $this->prize_id,
            'winner_date' => $this->winner_date,
            'send_status_id' => $this->send_status_id,
        ]);

        $query->andFilterWhere(['like', 'hash_id', $this->hash_id])
            ->andFilterWhere(['like', 'user_email', $this->user_email])
            ->andFilterWhere(['like', 'user_name', $this->user_name])
            ->andFilterWhere(['like', 'user_phone', $this->user_phone])
            ->andFilterWhere(['like', 'sms_check_code', $this->sms_check_code])
            ->andFilterWhere(['like', 'number', $this->number])
            ->andFilterWhere(['like', 'check_image', $this->check_image])
            ->andFilterWhere(['like', 'image_hash', $this->image_hash])
            ->andFilterWhere(['like', 'reason_rejection', $this->reason_rejection])
            ->andFilterWhere(['like', 'send_number', $this->send_number])
            ->andFilterWhere(['like', 'user_ip', $this->user_ip]);

        return $dataProvider;
    }
}
