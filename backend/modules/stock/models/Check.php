<?php
namespace backend\modules\stock\models;

use Yii;
use common\models\stock\Check as BaseCheck;
use backend\modules\user\models\User;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use DateTime;

class Check extends BaseCheck
{

    public function getViewAttributes()
    {
        return [
            [
                'attribute' => 'store_id',
                'value' => function($model){ return ($model->store) ? $model->store->name : NULL; },
            ],
            [
                'attribute' => 'user_id',
                'value' => function($model){ return $model->user_name; },
            ],
            'number',
            'check_date',
            [
                'attribute' => 'check_image',
                'value' => function($model){ return Html::img('/' . call_user_func([$model, 'getFilePath']) . '/' . $model->check_image, ['class' => 'img-responsive']);},
                'format' => 'html',
            ],
            'register_date',
            [
                'attribute' => 'status',
                'value' => function($model){ return $model->getStringStatus(); },
            ],
            'reason_rejection',
        ];
    }

    public function getFormElements()
    {
        $fields =  [
            'status'            => ['type' => 'dropdownlist', 'items' => self::getListStatuses()],
            'reason_rejection'  => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
            'send_number'       => ['type' => 'text'],
            'send_status_id'    => ['type' => 'dropdownlist', 'items' => $this->getListSendStatuses(), 'attributes' => ['prompt' => '']],
        ];

        if ($this->status === self::STATUS_PROCESSING) {
            unset($fields[ 'send_number' ]);
            unset($fields[ 'send_status_id' ]);
        }
        if ($this->status === self::STATUS_REJECT) {
            $fields = [];
        }
        if ($this->status === self::STATUS_ACCEPT) {
            unset($fields[ 'status' ]);
            unset($fields[ 'reason_rejection' ]);
        }

        return $fields;
    }

    public static function getListStatuses()
    {
        return [
            self::STATUS_PROCESSING => 'В обработке',
            self::STATUS_REJECT => 'Отклонен',
            self::STATUS_ACCEPT => 'Принят',
        ];
    }

    public function getStringStatus()
    {
        switch ($this->status) {
            case self::STATUS_PROCESSING:
                return 'В обработке';
            case self::STATUS_REJECT:
                return 'Отклонен';
            case self::STATUS_ACCEPT:
                return 'Принят';
            default:
                return NULL;
        }
    }

    private function getListSendStatuses()
    {
        return ArrayHelper::map(SendStatus::find()->all(), 'id', 'name');
    }

    public function getStore()
    {
        return $this->hasOne(Store::className(), ['id' => 'store_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getPrize()
    {
        return $this->hasOne(Prize::className(), ['id' => 'prize_id']);
    }

    public function getSendStatusString()
    {
        return ($this->sendStatus) ? $this->sendStatus->name : NULL;
    }

    public function getSendStatus()
    {
        return $this->hasOne(SendStatus::className(), ['id' => 'send_status_id']);
    }

    public function attributeLabels()
    {
        return [
            'store_id' => 'Торговая точка',
            'user_id' => 'ФИО пользователя',
            'number' => 'Номер в чеке',
            'check_date' => 'Дата в чеке',
            'check_image' => 'Изображение чека',
            'prize_id' => 'Приз',
            'winner_date' => 'Дата выиграша',
            'status' => 'Статус чека',
            'send_status_id' => 'Статус отправки приза',
            'hash_id' => 'хеш id розыграша',
            'reason_rejection' => 'Причина отклонения',
            'register_date' => 'Дата регистрации чека',
            'send_number' => 'ШПИ отправки',
            'user_email' => 'Указанный email',
            'user_name' => 'Указанное имя',
            'user_phone' => 'Указанный телефон',
        ];
    }

    public function getNewWinner()
    {
        Yii::$app->log->traceLevel = 0;
        if ($this->prize_id === self::BACKPACK_PRIZE_ID) {
            $this->getNewBackpackWinner();
        } else if ($this->prize_id === self::TABLET_PRIZE_ID) {
            $this->getNewTabletWinner();
        }
    }

    private function getNewBackpackWinner()
    {
        Yii::info(str_repeat('=', 50), 'winner');
        $currentDate  = $this->winner_date;
        Yii::info(date('d-m-Y H:i:s') . " Начинаем перевыбор победителя призаа Рюкзак за дату = {$currentDate}", 'winner');
        $date         = new DateTime($currentDate);

        $date->modify('-1 day');

        $winnerDate = $date->format('Y-m-d');

        $query = self::find()
                          ->andWhere(['between', 'register_date', $winnerDate, $currentDate])
                          ->orderBy(['register_date' => SORT_ASC]);

        Yii::info("Получаем список призов и идем по нему циклом", 'winner');

        $countChecks  = $query->count();
        $hashId = md5(time() . Yii::$app->security->generateRandomString());

        Yii::info("Разыгрываем приз с HASH ID {$hashId}", 'winner');
        Yii::info("Чеков зарегистрировано: {$countChecks}", 'winner');

        if (!$countChecks) {
            return FALSE;
        }

        $checks       = $query->all();
        $winner       = ($countChecks * (1 + tan($countChecks) + $countChecks)) % $countChecks;

        $this->hash_id      = NULL;

        $this->save(FALSE);

        while ($checks) {
            if (isset($checks[ $winner ])) {
                $currentCheck = $checks[ $winner ];
                unset($checks[ $winner ]);
                if ($currentCheck->prize_id) {
                    Yii::info("Юзер с чеком с ID {$currentCheck->id} уже получал приз 1 раз", 'winner');
                    $winner++;
                    continue;
                }
                $currentCheck->winner_date = $currentDate;
                $currentCheck->prize_id    = self::BACKPACK_PRIZE_ID;
                $currentCheck->hash_id     = $hashId;

                $currentCheck->save(FALSE);

                Yii::info("Победил чек с следующим номером: {$winner} с ID {$currentCheck->id}", 'winner');

                return FALSE;
            } else {
                if (!$checks) {
                    return FALSE;
                }
                $winner = 0;
            }
        }
    }

    private function getNewTabletWinner()
    {
        Yii::info(str_repeat('=', 50), 'winner');
        $currentDate = $this->winner_date;

        Yii::info(date('d-m-Y H:i:s') . " Начинаем перевыбор победителя призаа Планшет за дату = {$currentDate}", 'winner');

        $query = self::find()
                            ->orderBy(['register_date' => SORT_ASC]);

        $prevWinnerDate = TabletWinnerDate::find()
                                                ->where(['<', 'date', $currentDate])
                                                ->orderBy(['date' => SORT_ASC])
                                                ->one();

        if ($prevWinnerDate) {
            $query->andWhere(['between', 'register_date', $prevWinnerDate->date, $currentDate]);
        }

        $countChecks  = $query->count();

        Yii::info("Получаем список призов и идем по нему циклом", 'winner');

        $hashId = md5(time() . Yii::$app->security->generateRandomString());

        Yii::info("Разыгрываем приз с HASH ID {$hashId}", 'winner');
        Yii::info("Чеков зарегистрировано: {$countChecks}", 'winner');

        if (!$countChecks) {
            return FALSE;
        }

        $checks       = $query->all();
        $winner       = ($countChecks * (1 + tan($countChecks) + $countChecks)) % $countChecks;

        $this->hash_id      = NULL;

        $this->save(FALSE);

        while ($checks) {
            if (isset($checks[ $winner ])) {
                $currentCheck = $checks[ $winner ];
                unset($checks[ $winner ]);
                if ($currentCheck->prize_id) {
                    Yii::info("Юзер с чеком с ID {$currentCheck->id} уже получал приз 1 раз", 'winner');
                    $winner++;
                    continue;
                }
                $currentCheck->winner_date = $currentDate;
                $currentCheck->prize_id    = self::TABLET_PRIZE_ID;
                $currentCheck->hash_id     = $hashId;

                $currentCheck->save(FALSE);

                Yii::info("Победил чек с следующим номером: {$winner} с ID {$currentCheck->id}", 'winner');

                return FALSE;
            } else {
                if (!$checks) {
                    return FALSE;
                }
                $winner = 0;
            }
        }
    }

    public function checkStatus()
    {
        if ($this->user_email && $this->status == self::STATUS_REJECT) {
            //throw new \Exception("Error Processing Request", 1);

            Yii::$app->mailer->compose()
                             ->setFrom([Yii::$app->params[ 'supportEmail' ] => 'Support'])
                             ->setTextBody('Ваш чек с номером ' . $this->number . ' отклонен. Причина отклонения: ' . $this->reason_rejection)
                             ->setTo($this->user_email)
                             ->setSubject('Ваш чек отклонен')
                             ->send();
        }
    }

}