<?php
namespace backend\modules\stock\models;

use Yii;
use common\models\stock\Collection as BaseCollection;
use yii\helpers\ArrayHelper;

class Collection extends BaseCollection
{
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'image' => [
                    'class'       => '\backend\modules\core\behaviors\UploadImageBehavior',
                    'attribute'   => 'image',
                    'uploadPath'  => Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . call_user_func([$this, 'getFilePath']),
                ],
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
              [['display_order', 'count'], 'default', 'value' => 0],
              [['image'], 'safe'],
          ]);
    }

    public function getFormElements()
    {
        $fields =  [
            'name'          => ['type' => 'text'],
            'count'         => ['type' => 'text'],
            'image'         => ['type' => 'image', 'deleteImageUrl' => ['/stock/collection/delete-image', 'id' => $this->id]],
            'display_order' => ['type' => 'text'],
            'active'        => ['type' => 'checkbox'],
        ];

        return $fields;
    }

    public function getViewAttributes()
    {
        return ['id', 'name', 'count', 'image', 'display_order', 'active'];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название коллекции',
            'image' => Yii::t('core', 'Image'),
            'count' => Yii::t('core', 'Count'),
            'display_order' => Yii::t('core', 'Display order'),
            'active' => Yii::t('core', 'Active'),
        ];
    }
}