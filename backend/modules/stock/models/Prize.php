<?php
namespace backend\modules\stock\models;

use Yii;
use common\models\stock\Prize as BasePrize;
use yii\helpers\ArrayHelper;

class Prize extends BasePrize
{

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                'image' => [
                    'class'       => '\backend\modules\core\behaviors\UploadImageBehavior',
                    'attribute'   => 'image',
                    'uploadPath'  => Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . call_user_func([$this, 'getFilePath']),
                ],
            ]
        );
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
              [['display_order'], 'default', 'value' => 0],
              [['image'], 'safe'],
          ]);
    }

    public function getFormElements()
    {
        $fields =  [
            'name'          => ['type' => 'text'],
            'image'         => ['type' => 'image', 'deleteImageUrl' => ['/stock/prize/delete-image', 'id' => $this->id]],
            'display_order' => ['type' => 'text'],
            'active'        => ['type' => 'checkbox'],
        ];

        return $fields;
    }

    public function getViewAttributes()
    {
        return ['id', 'name', 'image', 'display_order', 'active'];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название приза',
            'image' => Yii::t('core', 'Image'),
            'display_order' => Yii::t('core', 'Display order'),
            'active' => Yii::t('core', 'Active'),
        ];
    }

}