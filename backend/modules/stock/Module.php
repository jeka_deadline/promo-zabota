<?php
namespace backend\modules\stock;

use Yii;
use yii\base\Module as BaseModule;

class Module extends BaseModule
{

    public $controllerNamespace = 'backend\modules\stock\controllers';

    public function init()
    {
        parent::init();
        //$this->registerTranslations();
    }

    /*public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations[ 'user' ])) {
            Yii::$app->i18n->translations[ 'user' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/user/messages',
            ];
        }

        if (!isset(Yii::$app->i18n->translations[ 'profile' ])) {
            Yii::$app->i18n->translations[ 'profile' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/profile/messages',
            ];
        }
    }*/

    public function getMenuItems()
    {
        return [
            [
                'label' => 'Торговые точки',
                'url' => ['/stock/store/index'],
                'icon' => '<i class="fa fa-university" aria-hidden="true"></i>',
            ],
            [
                'label' => 'Управление призами',
                'url' => ['/stock/prize/index'],
                'icon' => '<i class="fa fa-usd" aria-hidden="true"></i>',
            ],
            /*[
                'label' => 'Управление датами разыгрывания планшета',
                'url' => ['/stock/tablet-winner-date/index'],
                'icon' => '<i class="fa fa-calendar" aria-hidden="true"></i>',
            ],*/
            [
                'label' => 'Статусы отправки призов',
                'url' => ['/stock/send-status/index'],
                'icon' => '<i class="fa fa-truck" aria-hidden="true"></i>',
            ],
            [
                'label' => 'Коллекции',
                'url' => ['/stock/collection/index'],
                'icon' => '<i class="fa fa-newspaper-o" aria-hidden="true"></i>',
            ],
            [
                'label' => 'Управление питомцами',
                'url' => ['/stock/pet/index'],
                'icon' => '<i class="fa fa-newspaper-o" aria-hidden="true"></i>',
            ],
            [
                'label' => 'Чеки',
                'url' => ['/stock/check/index'],
                'icon' => '<i class="fa fa-newspaper-o" aria-hidden="true"></i>',
            ],
        ];
    }

}