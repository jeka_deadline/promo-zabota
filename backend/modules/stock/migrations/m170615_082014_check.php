<?php

use yii\db\Migration;
use yii\db\Expression;

class m170615_082014_check extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%stock_prizes}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(100)->notNull(),
            'image'         => $this->string(255)->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        $this->createTable('{{%stock_send_statuses}}', [
            'id'    => $this->primaryKey(),
            'name'  => $this->string(50)->notNull(),
        ]);

        $this->createTable('{{%stock_store}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(100)->notNull(),
            'image'         => $this->string(255)->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        $this->createTable('{{%stock_check}}', [
            'id'                => $this->primaryKey(),
            'hash_id'           => $this->string(32)->defaultValue(NULL),
            'store_id'          => $this->integer(11)->defaultValue(NULL),
            'user_id'           => $this->integer(11)->defaultValue(NULL),
            'user_email'        => $this->string(100)->defaultValue(NULL),
            'user_name'         => $this->string(100)->defaultValue(NULL),
            'user_phone'        => $this->string(15)->defaultValue(NULL),
            'sms_check_code'    => $this->string(255)->defaultValue(NULL),
            'attach_sms_code'   => $this->string(32)->defaultValue(NULL),
            'number'            => $this->string(50)->notNull(NULL),
            'check_date'        => $this->timestamp()->defaultValue(NULL),
            'check_image'       => $this->text()->defaultValue(NULL),
            'image_hash'        => $this->string(32)->defaultValue(NULL),
            'register_date'     => $this->timestamp()->defaultValue(NULL),
            'status'            => $this->smallInteger(1)->defaultValue(0),
            'reason_rejection'  => $this->text()->defaultValue(NULL),
            'prize_id'          => $this->integer(11)->defaultValue(NULL),
            'collection_id'     => $this->integer(11)->defaultValue(NULL),
            'winner_date'       => $this->timestamp()->defaultValue(NULL),
            'send_status_id'    => $this->integer(11)->defaultValue(NULL),
            'send_number'       => $this->string(50)->defaultValue(NULL),
            'user_ip'           => $this->string(15)->defaultValue(NULL),
        ]);

        $this->createTable('{{%stock_pets}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(100)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        $this->createTable('{{%stock_collections}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(100)->notNull(),
            'image'         => $this->string(255)->defaultValue(NULL),
            'count'         => $this->integer()->defaultValue(0),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);


        $this->createRelations();
    }

    public function safeDown()
    {
        $this->dropRelations();

        $this->dropTable('{{%stock_store}}');
        $this->dropTable('{{%stock_check}}');
        $this->dropTable('{{%stock_prizes}}');
        $this->dropTable('{{%stock_send_statuses}}');
        $this->dropTable('{{%stock_pets}}');
        $this->dropTable('{{%stock_collections}}');
    }

    private function createRelations()
    {
        $this->createIndex('ix_stock_check_store_id', '{{%stock_check}}', 'store_id');
        $this->addForeignKey('fk_stock_check_store_id', '{{%stock_check}}', 'store_id', '{{%stock_store}}', 'id', 'SET NULL', 'CASCADE');

        $this->createIndex('ix_stock_check_user_id', '{{%stock_check}}', 'user_id');
        $this->addForeignKey('fk_stock_check_user_id', '{{%stock_check}}', 'user_id', '{{%user_users}}', 'id', 'SET NULL', 'CASCADE');

        $this->createIndex('ix_stock_check_prize_id', '{{%stock_check}}', 'prize_id');
        $this->addForeignKey('fk_stock_check_prize_id', '{{%stock_check}}', 'prize_id', '{{%stock_prizes}}', 'id', 'SET NULL', 'CASCADE');

        $this->createIndex('ix_stock_check_send_status_id', '{{%stock_check}}', 'send_status_id');
        $this->addForeignKey('fk_stock_check_send_status_id', '{{%stock_check}}', 'send_status_id', '{{%stock_send_statuses}}', 'id', 'SET NULL', 'CASCADE');

        $this->createIndex('ix_stock_check_collection_id', '{{%stock_check}}', 'collection_id');
        $this->addForeignKey('fk_stock_check_collection_id', '{{%stock_check}}', 'collection_id', '{{%stock_collections}}', 'id', 'SET NULL', 'CASCADE');
    }

    private function dropRelations()
    {
        $this->dropForeignKey('fk_stock_check_store_id', '{{%stock_check}}');
        $this->dropForeignKey('fk_stock_check_user_id', '{{%stock_check}}');
        $this->dropForeignKey('fk_stock_check_prize_id', '{{%stock_check}}');
        $this->dropForeignKey('fk_stock_check_send_status_id', '{{%stock_check}}');
        $this->dropForeignKey('fk_stock_check_collection_id', '{{%stock_check}}');

        $this->dropIndex('ix_stock_check_store_id', '{{%stock_check}}');
        $this->dropIndex('ix_stock_check_user_id', '{{%stock_check}}');
        $this->dropIndex('ix_stock_check_prize_id', '{{%stock_check}}');
        $this->dropIndex('ix_stock_check_send_status_id', '{{%stock_check}}');
        $this->dropIndex('ix_stock_check_collection_id', '{{%stock_check}}');

    }
}
