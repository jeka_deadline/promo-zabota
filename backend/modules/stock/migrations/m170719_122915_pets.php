<?php

use yii\db\Migration;

class m170719_122915_pets extends Migration
{
    public function safeUp()
    {
        $this->createIndex('ix_user_profiles_pet_id', '{{%user_profiles}}', 'pet_id');
        $this->addForeignKey('fk_user_profiles_pet_id', '{{%user_profiles}}', 'pet_id', '{{%stock_pets}}', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_user_profiles_pet_id', '{{%user_profiles}}');
        $this->dropIndex('ix_user_profiles_pet_id', '{{%user_profiles}}');
    }
}