<?php
namespace backend\modules\stock\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class CollectionController extends BackendController
{

    public $modelName   = 'backend\modules\stock\models\Collection';
    public $searchModel = 'backend\modules\stock\models\searchModels\CollectionSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete', 'delete-image'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'backend\modules\core\components\CRUDIndex',
                'title' => 'Список коллекций',
            ],
            'create' => [
                'class'     => 'backend\modules\core\components\CRUDCreate',
                'title'     => 'Добавить коллекцию',
                'modelName' => $this->modelName,
            ],
            'update' => [
                'class'     => 'backend\modules\core\components\CRUDUpdate',
                'title'     => 'Обновить коллекцию',
                'modelName' => $this->modelName,
            ],
            'view' => [
                'class'     => 'backend\modules\core\components\CRUDView',
                'title'     => 'Просмотр коллекции',
                'modelName' => $this->modelName,
            ],
            'delete' => [
                'class' => 'backend\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function actionDeleteImage($id)
    {
        $model = call_user_func_array([$this->modelName, 'findOne'], ['id' => $id]);

        if (!$model) {
            throw new NotFoundHttpException('Приз');
        }

        $file = Yii::getAlias('@frontend/web/') . call_user_func([$this->modelName, 'getFilePath']) . '/' . $model->image;

        if (file_exists($file) && is_file($file) && is_writable($file)) {
            unlink($file);
            $model->updateAttributes(['image' => NULL]);
        }

        return $this->redirect(['update', 'id' => $id]);
    }

    public function getColumns()
    {
        return [
            $this->getGridSerialColumn(),
            ['attribute' => 'name'],
            ['attribute' => 'count'],
            ['attribute' => 'display_order'],
            $this->getGridActive(),
            $this->getGridActions(),
        ];
    }

}