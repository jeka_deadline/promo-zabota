<?php
use yii\widgets\DetailView;
use yii\helpers\Html;
?>

<?= Html::a(Yii::t('core', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>

<br>
<br>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'login',
        'email',
        [
            'label' => Yii::t('user', 'Access to admin panel'),
            'value' => ($model->is_admin) ? Yii::t('user', 'Allowed') : Yii::t('user', 'Forbidden'),
        ],
        [
            'label' => Yii::t('user', 'Is user block'),
            'value' => ($model->blocked_at) ? Yii::t('core', 'Yes') : Yii::t('core', 'No'),
        ],
        [
            'label' => Yii::t('user', 'Is user confirm email'),
            'value' => ($model->confirm_email_at) ? Yii::t('core', 'Yes') : Yii::t('core', 'No'),
        ],
        [
            'label' => Yii::t('profile', 'Surname'),
            'value' => $model->profile->surname,
        ],
        [
            'label' => Yii::t('profile', 'Name'),
            'value' => $model->profile->name,
        ],
        [
            'label' => Yii::t('profile', 'Patronymic'),
            'value' => $model->profile->patronymic,
        ],
        [
            'label' => Yii::t('profile', 'Phone'),
            'value' => $model->profile->phone,
        ],
        [
            'label' => Yii::t('profile', 'Skype'),
            'value' => $model->profile->skype,
        ],
        [
            'label' => Yii::t('profile', 'Date birth'),
            'value' => $model->profile->date_birth,
        ],
    ],
]); ?>