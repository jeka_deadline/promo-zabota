<?php
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<?= Html::a(Yii::t('user', 'Create user'), ['create'], ['class' => 'btn btn-success']); ?>

<br>
<br>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'attribute' => 'email',
        ],
        [
            'attribute' => 'login',
        ],
        [
            'format'    => 'raw',
            'header'    => Yii::t('user', 'Security action'),
            'value'     => function($model) use ($currentUserId)
            {
                if ($model->id === $currentUserId) {
                    return FALSE;
                }

                $optionsRole = $optionsBlock = [
                    'class' => 'btn btn-xs',
                ];

                if ($model->is_admin) {
                    $textBtnRole  = Yii::t('user', 'Remove admin role');
                    $classBtnRole = 'btn-danger';
                } else {
                    $textBtnRole  = Yii::t('user', 'Add admin role');
                    $classBtnRole = 'btn-success';
                }

                if ($model->blocked_at) {
                    $textBtnBlock  = Yii::t('user', 'Unblock user');
                    $classBtnBlock = 'btn-danger';
                } else {
                    $textBtnBlock  = Yii::t('user', 'Block user');
                    $classBtnBlock = 'btn-success';
                }

                Html::addCssClass($optionsRole, $classBtnRole);
                Html::addCssClass($optionsBlock, $classBtnBlock);

                return
                    '<div class="btn-group">' .
                        Html::a($textBtnRole, Url::toRoute(['/user/user/update-user-role', 'id' => $model->id]), $optionsRole) .
                        Html::a($textBtnBlock, Url::toRoute(['/user/user/update-user-block', 'id' => $model->id]), $optionsBlock) .
                    '</div>';
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('core', 'Actions'),
        ],
    ],
]);