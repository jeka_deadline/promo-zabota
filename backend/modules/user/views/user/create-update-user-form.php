<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use backend\modules\user\models\forms\CreateUserForm;
?>

<?php $form = ActiveForm::begin(); ?>

    <?= Tabs::widget([
        'items' => [
            [
                'label'   => Yii::t('user', 'User information'),
                'content' => $this->render('_block_user_information', [
                    'form' => $form,
                    'model' => $model,
                ]),
            ],
            [
                'label'   => Yii::t('user', 'Profile information'),
                'content' => $this->render('_block_profile_information', [
                    'form' => $form,
                    'model' => $model,
                ]),
            ]
        ],
    ]); ?>

    <?= Html::submitButton(($model->getScenario() === CreateUserForm::SCENARIO_CREATE) ? Yii::t('user', 'Create user') : Yii::t('user', 'Update user'), [
        'class' => 'btn btn-primary'
    ]); ?>
    <?= Html::a(Yii::t('core', 'Cancel'), ['index'], ['class' => 'btn btn-default']); ?>

<?php ActiveForm::end(); ?>