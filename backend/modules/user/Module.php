<?php
namespace backend\modules\user;

use Yii;
use yii\base\Module as BaseModule;

class Module extends BaseModule
{

    public $controllerNamespace = 'backend\modules\user\controllers';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations[ 'user' ])) {
            Yii::$app->i18n->translations[ 'user' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/user/messages',
            ];
        }

        if (!isset(Yii::$app->i18n->translations[ 'profile' ])) {
            Yii::$app->i18n->translations[ 'profile' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/profile/messages',
            ];
        }
    }

    public function getMenuItems()
    {
        return [
            /*[
                'label' => 'Пользователи',
                'items' => [
                    [
                        'label' => Yii::t('user', 'Пользователи'),
                        'url'   => ['/user/user/index'],
                    ],
                ],
            ]*/
        ];
    }

}