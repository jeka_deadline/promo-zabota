<?php

use yii\db\Migration;
use yii\db\Expression;

class m160331_140545_users extends Migration
{
    public function safeUp()
    {

      // таблица пользователей
      $this->createTable('{{%user_users}}', [
          'id'                    => $this->primaryKey(),
          'is_admin'              => $this->smallInteger(1)->defaultValue(0),
          'email'                 => $this->string(100)->notNull(),
          'temporary_code'        => $this->string(32)->defaultValue(NULL),
          'password_hash'         => $this->string(100)->notNull(),
          'reset_password_token'  => $this->string(32)->defaultValue(NULL),
          'auth_key'              => $this->string(32)->notNull(),
          'blocked_at'            => $this->timestamp()->defaultValue(NULL),
          'confirm_email_at'      => $this->timestamp()->defaultValue(NULL),
          'confirm_phone_at'      => $this->timestamp()->defaultValue(NULL),
          'register_ip'           => $this->string(15)->notNull(),
          'created_at'            => $this->timestamp()->defaultValue(NULL),
          'updated_at'            => $this->timestamp()->defaultValue(NULL),
          'login_with_social'     => $this->smallInteger(1)->defaultValue(0),
      ]);

      // таблица профиля пользователя
      $this->createTable('{{%user_profiles}}', [
          'id'          => $this->primaryKey(),
          'user_id'     => $this->integer(11)->notNull(),
          'surname'     => $this->string(100)->defaultValue(NULL),
          'name'        => $this->string(100)->defaultValue(NULL),
          'date_birth'  => $this->date()->defaultValue(NULL),
          'phone'       => $this->string(15)->defaultValue(NULL),
          'city'        => $this->string(100)->defaultValue(NULL),
          'region'      => $this->string(100)->defaultValue(NULL),
          'address'     => $this->string(255)->defaultValue(NULL),
          'pet_id'      => $this->integer(11)->defaultValue(NULL),
      ]);

      // таблица соц. аккаунтов пользователя
      $this->createTable('{{%user_socials}}', [
          'id'          => $this->primaryKey(),
          'provider'    => $this->string(100)->notNull(),
          'client_id'   => $this->string(100)->notNull(),
          'created_at'  => $this->timestamp()->defaultValue(NULL),
          'user_id'     => $this->integer(11)->notNull(),
      ]);

      $this->createTable('{{%user_notifications}}', [
          'id' => $this->primaryKey(),
          'user_id' => $this->integer(11)->notNull(),
          'text' => $this->text()->notNull(),
          'is_new' => $this->smallInteger(1)->defaultValue(1),
          'created_at' => $this->timestamp()->defaultValue(new Expression('NOW()')),
      ]);

        // таблица логирования профиля пользователя
        $this->createTable('{{%user_log_profiles}}', [
            'id'                => $this->primaryKey(),
            'user_id'           => $this->integer(11)->notNull(),
            'surname'           => $this->string(100)->defaultValue(NULL),
            'name'              => $this->string(100)->defaultValue(NULL),
            'date_birth'        => $this->date()->defaultValue(NULL),
            'phone'             => $this->string(15)->defaultValue(NULL),
            'city'              => $this->string(100)->defaultValue(NULL),
            'region'            => $this->string(100)->defaultValue(NULL),
            'address'           => $this->string(255)->defaultValue(NULL),
            'code_change_phone' => $this->string(10)->defaultValue(NULL),
        ]);

      $this->createRelations();
      $this->createAdmins();

    }

    public function safeDown()
    {
        $this->removeRelations();
        $this->dropTable('{{%user_notifications}}');
        $this->dropTable('{{%user_socials}}');
        $this->dropTable('{{%user_profiles}}');
        $this->dropTable('{{%user_users}}');
        $this->dropTable('{{%user_log_profiles}}');
    }

    private function createRelations()
    {
        $this->createIndex('uix_user_users_email', '{{%user_users}}', 'email', TRUE);

        // создание связи таблицы соц. аккаунтов пользователя с таблицей пользователей
        $this->createIndex('ix_user_socials_user_id', '{{%user_socials}}', 'user_id');
        $this->addForeignKey('fk_user_socials_user_id', '{{%user_socials}}', 'user_id', '{{%user_users}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('ix_user_profiles_user_id', '{{%user_profiles}}', 'user_id');
        $this->addForeignKey('fk_user_profiles_user_id', '{{%user_profiles}}', 'user_id', '{{%user_users}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('ix_user_notifications_user_id', '{{%user_notifications}}', 'user_id');
        $this->addForeignKey('fk_user_notifications_user_id', '{{%user_notifications}}', 'user_id', '{{%user_users}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('ix_user_log_profiles_user_id', '{{%user_log_profiles}}', 'user_id');
        $this->addForeignKey('fk_user_log_profiles_user_id', '{{%user_log_profiles}}', 'user_id', '{{%user_users}}', 'id', 'CASCADE', 'CASCADE');
    }

    private function removeRelations()
    {
        $this->dropIndex('uix_user_users_email', '{{%user_users}}');

        $this->dropForeignKey('fk_user_socials_user_id', '{{%user_socials}}');
        $this->dropIndex('ix_user_socials_user_id', '{{%user_socials}}');

        $this->dropForeignKey('fk_user_profiles_user_id', '{{%user_profiles}}');
        $this->dropIndex('ix_user_profiles_user_id', '{{%user_profiles}}');

        $this->dropForeignKey('fk_user_notifications_user_id', '{{%user_notifications}}');
        $this->dropIndex('ix_user_notifications_user_id', '{{%user_notifications}}');

        $this->dropForeignKey('fk_user_log_profiles_user_id', '{{%user_log_profiles}}');
        $this->dropIndex('ix_user_log_profiles_user_id', '{{%user_log_profiles}}');
    }

    private function createAdmins()
    {
        $this->insert('{{user_users}}', [
            'email'                 => 'jeka.deadline@gmail.com',
            'is_admin'              => 1,
            'password_hash'         => \Yii::$app->getSecurity()->generatePasswordHash('admin'),
            'reset_password_token'  => NULL,
            'auth_key'              => md5(time() . \Yii::$app->getSecurity()->generateRandomString(50)),
            'confirm_email_at'      => new Expression('NOW()'),
            'confirm_phone_at'      => new Expression('NOW()'),
            'register_ip'           => '127.0.0.1',
            'created_at'            => new Expression('NOW()'),
            'updated_at'            => new Expression('NOW()'),
        ]);

        $this->insert('{{%user_profiles}}', [
            'user_id'     => 1,
            'surname'     => 'Бублик',
            'name'        => 'Евгений',
            'phone'       => '380935994767',
            'date_birth'  => '1992-05-14',
            'city'        => 'Чернигов',
            'region'      => 'Чернигов',
        ]);
    }

}
