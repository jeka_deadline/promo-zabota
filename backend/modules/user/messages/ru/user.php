<?php
    return [
        'User not found' => 'Пользователь не найден',
        'You can not change role for self' => 'Вы не можете изменить роль для себя',
        'You can not change block for self' => 'Вы не можете блокировать/разблокировать самого себя',
        'You can not delete self' => 'Вы не можете удалить самого себя',
        'User success delete' => 'Пользователь успешно удален',
        'Incorrect email or password.' => 'Неправильный email или пароль',
    ];
?>