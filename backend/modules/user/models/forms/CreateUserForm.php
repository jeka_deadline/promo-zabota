<?php
namespace backend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use backend\modules\user\models\User;
use backend\modules\user\models\Profile;
use yii\db\Expression;

class CreateUserForm extends Model
{

    // user fields
    public $email;
    public $password;
    public $repeatPassword;
    public $isAdmin;
    public $isConfirmEmail;
    public $isConfirmPhone;

    // profile fields
    public $surname;
    public $name;
    public $address;
    public $phone;
    public $city;
    public $region;
    public $dateBirth;

    private $_user;

    const SCENARIO_CREATE = 'create';

    public function __construct($user = NULL, $options = [])
    {
        if ($user instanceof User) {
            $this->_user = $user;

            $this->setFields($user);
        }

        parent::__construct($options);
    }

    public function rules()
    {
        return [
            // user rules
            [['email'], 'required'],
            [['password', 'repeatPassword'], 'required', 'on' => self::SCENARIO_CREATE],
            [['repeatPassword'], 'compare', 'compareAttribute' => 'password', 'on' => self::SCENARIO_CREATE],
            [['email'], 'filter', 'filter' => 'trim'],
            [['email'], 'string', 'max' => 100],
            [['email'], 'email'],
            [['email'], 'unique', 'targetClass' => User::className(), 'targetAttribute' => 'email', 'filter' => function($query) {
                if ($this->getScenario() === self::SCENARIO_CREATE) {
                    return $query;
                }

                return $query->andWhere(['<>', 'id', $this->_user->id]);
            }],
            [['password', 'repeatPassword'], 'string', 'max' => 100],
            [['isAdmin', 'isConfirmEmail', 'isConfirmPhone'], 'boolean'],

            // profile rules
            [['surname', 'name', 'city', 'dateBirth'], 'required'],
            [['surname', 'name', 'city', 'phone', 'region', 'address'], 'filter', 'filter' => 'trim'],
            [['surname', 'name', 'city', 'region'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 15],
            [['address'], 'string', 'max' => 255],
            [['dateBirth'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    public function createUser()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $user = new User();

        $user->email            = $this->email;
        $user->is_admin         = ($this->isAdmin) ? 1 : 0;
        $user->confirm_email_at = ($this->isConfirmEmail) ? new Expression('NOW()') : NULL;
        $user->confirm_phone_at = ($this->isConfirmPhone) ? new Expression('NOW()') : NULL;

        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->setRegisterIp();

        $connection = Yii::$app->db;

        $transaction = $connection->beginTransaction();

        if ($user->validate() && $user->save()) {
            $profile              = new Profile();
            $profile->attributes  = $this->attributes;
            $profile->date_birth  = $this->dateBirth;
            $profile->user_id     = $user->id;

            if ($profile->validate() && $profile->save()) {
                $transaction->commit();
                return TRUE;
            }

            $transaction->rollback();
        }

        return FALSE;
    }

    public function updateUser()
    {
        if (!$this->validate()) {
            return FALSE;
        }

        $user         = $this->_user;
        $user->email  = $this->email;

        if ($this->password) {
            $user->setPassword($this->password);
        }

        foreach ($user->profile->attributes as $attributeName => $attributeValue) {
            if (!property_exists($this, $attributeName)) {
                continue;
            }

            $user->profile->$attributeName = $this->$attributeName;
        }

        $user->profile->date_birth = $this->dateBirth;

        if ($user->save() && $user->profile->save()) {
            return TRUE;
        }

        return FALSE;

    }

    public function setFields(User $user)
    {
        $this->email = $user->email;

        foreach ($user->profile->attributes as $attributeName => $attributeValue) {
            if (!property_exists($this, $attributeName)) {
                continue;
            }

            $this->$attributeName = $attributeValue;
        }

        $this->dateBirth = $user->profile->date_birth;
    }

}