<?php
namespace backend\modules\user\models;

use Yii;
use common\models\user\User as BaseUser;

class User extends BaseUser
{

    public function isAdmin()
    {
        return $this->is_admin;
    }

    public static function findByEmailAdmin($email)
    {
        return static::findOne(['email' => $email, 'is_admin' => 1]);
    }

    // ================================================================== Relations =========================================================================

    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    public function getSocials()
    {
        return $this->hasMany(Social::className(), ['user_id' => 'id']);
    }

}