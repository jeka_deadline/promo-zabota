<?php
namespace backend\modules\user\models;

use Yii;
use common\models\user\Notification as BaseNotification;

class Notification extends BaseNotification
{

    public static function createUserNotification($userId, $text)
    {
        $model          = new static();
        $model->user_id = $userId;
        $model->text    = $text;

        if ($model->validate() && $model->save()) {
            return $model;
        }

        return FALSE;
    }

}