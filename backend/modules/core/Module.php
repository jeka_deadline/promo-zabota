<?php
namespace backend\modules\core;

use Yii;
use yii\base\Module as BaseModule;
use backend\modules\core\models\SupportMessage;

class Module extends BaseModule
{

    public $controllerNamespace = 'backend\modules\core\controllers';

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations[ 'core' ])) {
            Yii::$app->i18n->translations[ 'core' ] = [
                'class'           => 'yii\i18n\PhpMessageSource',
                'sourceLanguage'  => 'ru-Ru',
                'basePath'        => '@app/modules/core/messages',
            ];
        }
    }

    public function getMenuItems()
    {
        return [
            [
                'label' => Yii::t('core', 'Management site'),
                'icon' => '<i class="fa fa-dashboard"></i>',
                'items' => [
                    [
                        'label' => Yii::t('core', 'Management text blocks'),
                        'url'   => ['/core/text-block/index'],
                    ],
                    [
                        'label' => Yii::t('core', 'Management core pages'),
                        'url'   => ['/core/page/index'],
                    ],
                    [
                        'label' => Yii::t('core', 'Management params'),
                        'url'   => ['/core/params/index'],
                    ],
                    [
                        'label' => Yii::t('core', 'Support subjects'),
                        'url'   => ['/core/support-subject/index'],
                    ],
                    [
                        'label' => 'Готовые ответы',
                        'url'   => ['/core/ready-answers-support/index'],
                    ],
                    [
                        'label' => Yii::t('core', 'Support messages') . ' (' . SupportMessage::getNewSupportMessages() . ')',
                        'url' => ['/core/support-message/index'],
                    ]
                ],
            ]
        ];
    }

}
?>