<?php

use yii\db\Migration;
use yii\db\Expression;

class m170126_135301_core extends Migration
{
    public function safeUp()
    {
        // таблица текстовых блоков
        $this->createTable('{{%core_text_block}}', [
            'id'            => $this->primaryKey(),
            'title'         => $this->string(100)->notNull(),
            'description'   => $this->string(255)->defaultValue(NULL),
            'uri'           => $this->string(255)->notNull(),
            'content'       => $this->text()->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        // таблица страниц
        $this->createTable('{{%core_pages}}', [
            'id'            => $this->primaryKey(),
            'route'         => $this->string(255)->notNull(),
            'header'        => $this->string(255)->notNull(),
            'menu_class'    => $this->string(255)->defaultValue(NULL),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        // таблица мета-данных страниц
        $this->createTable('{{%core_pages_meta}}', [
            'id'                => $this->primaryKey(),
            'owner_id'          => $this->integer()->notNull(),
            'modelClass'        => $this->string(255)->notNull(),
            'meta_title'        => $this->string(255)->defaultValue(NULL),
            'meta_description'  => $this->text()->defaultValue(NULL),
            'meta_keywords'     => $this->text()->defaultValue(NULL),
        ]);

        // Таблица для параметров системы
        $this->createTable('{{%core_params}}', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string(255)->notNull(),
            'code'        => $this->string(255),
            'description' => $this->text()->defaultValue(NULL),
            'value'       => $this->text()->defaultValue(NULL),
        ]);

        $this->createTable('{{%core_support_subjects}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(100)->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);

        $this->createTable('{{%core_support_messages}}', [
            'id'              => $this->primaryKey(),
            'subject_id'      => $this->integer(11)->notNull(),
            'user_id'         => $this->integer(11)->defaultValue(NULL),
            'name'            => $this->string(100)->notNull(),
            'email'           => $this->string(100)->notNull(),
            'custom_subject'  => $this->string(255)->defaultValue(NULL),
            'text'            => $this->text()->notNull(),
            'answer'          => $this->text()->defaultValue(NULL),
            'image'           => $this->string(255)->defaultValue(NULL),
            'is_new'          => $this->smallInteger(1)->defaultValue(1),
            'date'            => $this->timestamp()->defaultValue(new Expression('NOW()')),
        ]);

        $this->createTable('{{%core_ready_answers_for_support}}', [
            'id'            => $this->primaryKey(),
            'header'        => $this->string(100)->notNull(),
            'text'          => $this->text()->notNull(),
            'display_order' => $this->integer()->defaultValue(0),
        ]);

        $this->createRelations();
        $this->insertCorePages();
    }

    private function createRelations()
    {

        $this->createIndex('uix_core_text_block_uri', '{{%core_text_block}}', 'uri', TRUE);
        $this->createIndex('uix_core_pages_route', '{{%core_pages}}', 'route', TRUE);
        $this->createIndex('uix_core_params_code', '{{%core_params}}', 'code', TRUE);

        $this->createIndex('ix_core_support_messages_subject_id', '{{%core_support_messages}}', 'subject_id');
        $this->addForeignKey('fk_core_support_messages_subject_id', '{{%core_support_messages}}', 'subject_id', '{{%core_support_subjects}}', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('ix_core_support_messages_user_id', '{{%core_support_messages}}', 'user_id');
        $this->addForeignKey('fk_core_support_messages_user_id', '{{%core_support_messages}}', 'user_id', '{{%user_users}}', 'id', 'SET NULL', 'CASCADE');
    }

    private function removeRelations()
    {
        $this->dropIndex('uix_core_text_block_uri', '{{%core_text_block}}');
        $this->dropIndex('uix_core_pages_route', '{{%core_pages}}');
        $this->dropIndex('uix_core_params_code', '{{%core_params}}');

        $this->dropForeignKey('fk_core_support_messages_subject_id', '{{%core_support_messages}}');
        $this->dropIndex('ix_core_support_messages_subject_id', '{{%core_support_messages}}');

        $this->dropForeignKey('fk_core_support_messages_user_id', '{{%core_support_messages}}');
        $this->dropIndex('ix_core_support_messages_user_id', '{{%core_support_messages}}');
    }

    private function insertCorePages()
    {
        $this->insert('{{%core_pages}}', [
            'route'   => 'core/index/index',
            'header'  => 'Главная страница',
            'active'  => 1,
        ]);
    }

    public function safeDown()
    {
        $this->removeRelations();

        $this->dropTable('{{%core_params}}');
        $this->dropTable('{{%core_pages_meta}}');
        $this->dropTable('{{%core_pages}}');
        $this->dropTable('{{%core_text_block}}');
        $this->dropTable('{{%core_support_messages}}');
        $this->dropTable('{{%core_support_subjects}}');
        $this->dropTable('{{%core_ready_answers_for_support}}');
    }
}