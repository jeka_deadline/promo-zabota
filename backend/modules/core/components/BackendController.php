<?php
namespace backend\modules\core\components;

use Yii;
use yii\web\Controller;
use yii\helpers\ArrayHelper;

class BackendController extends Controller
{
    protected function getGridSerialColumn()
    {
        return ['class' => 'yii\grid\SerialColumn'];
    }

    protected function getGridActions($options = [])
    {
        $buttons = [
            'class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('core', 'Actions'),
        ];

        return ArrayHelper::merge($buttons, $options);
    }

    protected function getGridActive($attribute = 'active')
    {
        return [
            'attribute' => $attribute,
            'filter'    => ['0' => Yii::t('core', 'No'), '1' => Yii::t('core', 'Yes')],
            'value'     => function($model) use ($attribute) {
                              return ($model->$attribute) ? '<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>' : '<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>';
            },
            'format'    => 'html',
        ];
    }

    protected function getGridColumnYesNo($attribute)
    {
        return [
            'attribute' => $attribute,
            'filter'    => ['0' => Yii::t('core', 'No'), '1' => Yii::t('core', 'Yes')],
            'value' => function($model){ return ($model->$attribute) ? Yii::t('core', 'Yes') : Yii::t('core', 'No');},
        ];
    }

}