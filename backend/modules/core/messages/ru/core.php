<?php
    return [
        'Yes' => 'Да',
        'No' => 'Нет',
        'Back' => 'Назад',
        'Create' => 'Создать',
        'Update' => 'Обновить',
        'Delete' => 'Удалить',
        'Actions' => 'Действия',
        'Active' => 'Активно',
        'Display order' => 'Порядок отображения',

        // Перевод меню
        'Management site' => 'Управление сайтом',
        'Attachemnt files' => 'Файловый менеджер',
        'Management text blocks' => 'Управление текстовыми блоками',
        'Management сore pages' => 'Управление страницы системы',
        'Management social links' => 'Управление социальными ссылками',
        'Management params' => 'Управление параметрами системы',
        'Management types menu' => 'Управление типами меню',
        'Management mail templates' => 'Управление шаблонами сообщений',

        'List attachments' => 'Список файлов сайта',
        'Add attachment' => 'Добавить файл',

        'List mail templates' => 'Список шаблонов сообщений',
        'Create mail template' => 'Добавить шаблон сообщения',
        'Update mail template' => 'Обновить шаблон сообщения',
        'View mail template' => 'Просмотр шаблона сообщения',

        'List core pages' => 'Список страниц системы',
        'Update core page' => 'Обновить страницу системы',

        'List social links' => 'Список социальных ссылок',
        'Create social link' => 'Добавить социальную ссылку',
        'Update social link' => 'Обновить социальную ссылку',
        'View social link' => 'Просмотр социальной ссылки',

        'List text blocks' => 'Список текстовых блоков',
        'Create text block' => 'Добавить текстовый блок',
        'Update text block' => 'Обновить текстовый блок',
        'View text block' => 'Просмотр текстового блока',

        'List types menu' => 'Список типов меню',
        'Create type menu' => 'Добавить тип меню',
        'Update type menu' => 'Обновить тип меню',
        'View type menu' => 'Просмотр типа меню',

        'Image' => 'Изображение',
        'Is new' => 'Новое',

        'Management core pages' => 'Страницы системы',
        'Support subjects' => 'Темы для тех по',
        'Support messages' => 'Обращения в тех по',
    ];
?>