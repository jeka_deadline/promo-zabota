<?php
use yii\helpers\Html;
?>

<?= $headerContent; ?>

<div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('crud-form', [
        'model'             => $model,
        'formElements'      => $formElements,
        'activeFormConfig'  => $activeFormConfig,
    ]) ?>

</div>

<?= $footerContent; ?>