<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use backend\assets\SupportAsset;
use yii\helpers\Url;

SupportAsset::register($this);

?>

<?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'readyAnswers')->dropDownList($model->getListReadyAnswers(), ['class' => 'form-control', 'id' => 'js-select-ready-answer', 'prompt' => 'Свой ответ', 'data' => ['url' => Url::toRoute(['/core/ready-answers-support/get-ready-answer'])]]); ?>

    <?= $form->field($model, 'answer')->textarea(['rows' => 10, 'id' => 'textarea-answer']); ?>

    <?= Html::submitButton('Ответить', ['class' => 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>