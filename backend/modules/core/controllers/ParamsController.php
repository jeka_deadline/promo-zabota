<?php
namespace backend\modules\core\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use backend\modules\core\models\GroupParam;
use backend\modules\core\models\Param;

class ParamsController extends BackendController
{

    public $modelName   = 'backend\modules\core\models\Param';
    public $searchModel = 'backend\modules\core\models\searchModels\ParamSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->request->isPost) {

            Param::savePostParams(Yii::$app->request->post('Param'));

            return $this->redirect(['index']);
        }

        $groupsParams = GroupParam::find()
                                        ->with('params', 'params.type')
                                        ->all();

        $tabs = [];

        foreach ($groupsParams as $key => $group) {

            $tabs[ $key ][ 'content' ]  = '<br>';
            $tabs[ $key ][ 'label' ]    = $group->name;

            foreach ($group->params as $param) {
                switch ($param->type->name_field) {
                    case 'checkbox':
                        $tabs[ $key ][ 'content' ] .= $this->renderPartial('forms-field/checkbox', ['param' => $param]);
                        break;

                    case 'text':
                        $tabs[ $key ][ 'content' ] .= $this->renderPartial('forms-field/text', ['param' => $param]);
                        break;

                    case 'textarea':
                        $tabs[ $key ][ 'content' ] .= $this->renderPartial('forms-field/textarea', ['param' => $param]);
                        break;
                }
            }
        }

        return $this->render('index', [
            'tabs' => $tabs,
        ]);
    }

}