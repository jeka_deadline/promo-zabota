<?php
namespace backend\modules\core\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class ReadyAnswersSupportController extends BackendController
{

    public $modelName   = 'backend\modules\core\models\ReadyAnswerForSupport';
    public $searchModel = 'backend\modules\core\models\searchModels\ReadyAnswerForSupportSearch';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'view', 'delete', 'get-ready-answer'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'backend\modules\core\components\CRUDIndex',
                'title' => 'Список готовых ответов для тех. поддержки',
            ],
            'create' => [
                'class'     => 'backend\modules\core\components\CRUDCreate',
                'title'     => 'Добавить готовый ответ для тех. поддержки',
                'modelName' => $this->modelName,
            ],
            'update' => [
                'class'     => 'backend\modules\core\components\CRUDUpdate',
                'title'     => 'Обновить готовый ответ для тех. поддержки',
                'modelName' => $this->modelName,
            ],
            'view' => [
                'class'     => 'backend\modules\core\components\CRUDView',
                'title'     => 'Просмотр готового ответа тех. поддержки',
                'modelName' => $this->modelName,
            ],
            'delete' => [
                'class' => 'backend\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
        ];
    }

    public function actionGetReadyAnswer($idReadyAnswer)
    {
        $model = call_user_func_array([$this->modelName, 'findOne'], [$idReadyAnswer]);

        if (!$model) {
            return NULL;
        }

        return $model->text;
    }

    public function getColumns()
    {
        return [
            $this->getGridSerialColumn(),
            ['attribute' => 'header'],
            ['attribute' => 'text'],
            $this->getGridActions(),
        ];
    }

}