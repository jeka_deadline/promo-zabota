<?php
namespace backend\modules\core\models;

use Yii;
use common\models\core\SupportMessage as BaseSupportMessage;
use yii\helpers\Html;

class SupportMessage extends BaseSupportMessage
{

    public function getViewAttributes()
    {
        return [
            'name',
            'email',
            [
                'attribute' => 'subject_id',
                'value' => function($model){ return $model->subject->name;},
            ],
            'text',
            [
                'attribute' => 'image',
                'value' => function($model){ return Html::img('/' . static::getFilePath() . DIRECTORY_SEPARATOR . $model->image, ['class' => 'img-responsive']);},
                'format' => 'html',
            ],
            'answer',
        ];
    }

    public function getStringIsNew()
    {
        switch ($this->is_new) {
            case 1:
                return Yii::t('core', 'Yes');
            case 0:
                return  Yii::t('core', 'No');
            default:
                return NULL;
        }
    }

    public static function getNewSupportMessages()
    {
        return static::find()
                            ->where(['is_new' => 1])
                            ->count();
    }

    public function getSubject()
    {
        return $this->hasOne(SupportSubject::className(), ['id' => 'subject_id']);
    }

    public function attributeLabels()
    {
        return [
            'subject_id' => 'Тема',
            'text' => 'Текст',
            'image' => Yii::t('core', 'Image'),
            'answer' => 'Ответ',
            'name' => 'Имя пользователя',
            'is_new' => Yii::t('core', 'Is new'),
            'custom_subject' => 'Тема пользователя',
        ];
    }

}