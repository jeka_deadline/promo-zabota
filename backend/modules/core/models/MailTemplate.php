<?php
namespace backend\modules\core\models;

use Yii;
use common\models\core\MailTemplate as BaseMailTemplate;
use yii\helpers\ArrayHelper;

class MailTemplate extends BaseMailTemplate
{

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
              [['code'], 'match', 'pattern' => '/^[a-z][a-z\d_-]+[a-z\d]$/i'],
          ]);
    }

    public function getFormElements()
    {
        $fields =  [
            'title'       => ['type' => 'text'],
            'code'        => ['type' => 'text'],
            'description' => ['type' => 'textarea', 'attributes' => ['rows' => 5]],
            //'text'        => ['type' => 'textarea', 'attributes' => ['rows' => 10]],
        ];

        return $fields;
    }

    public function getViewAttributes()
    {
        return ['id', 'title', 'code', 'description', 'text'];
    }

}