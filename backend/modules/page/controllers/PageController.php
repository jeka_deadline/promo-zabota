<?php

namespace backend\modules\page\controllers;

use Yii;
use backend\modules\core\components\BackendController;
use yii\filters\VerbFilter;
use zxbodya\yii2\elfinder\ConnectorAction;
use yii\filters\AccessControl;

/**
 * PageController implements the CRUD actions for Page model.
 */
class PageController extends BackendController
{

    public $searchModel = 'backend\modules\page\models\searchModels\PageSearch';
    public $modelName   = 'backend\modules\page\models\Page';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view', 'connector'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'index' => [
                'class'   => 'backend\modules\core\components\CRUDIndex',
                'title'   => Yii::t('page', 'Pages'),
            ],
            'create' => [
                'class'     => 'backend\modules\core\components\CRUDCreate',
                'title'     => Yii::t('page', 'Add page'),
                'modelName' => $this->modelName,
            ],
            'update' => [
                'class'     => 'backend\modules\core\components\CRUDUpdate',
                'title'     => Yii::t('page', 'Update page'),
                'modelName' => $this->modelName,
            ],
            'view' => [
                'class'     => 'backend\modules\core\components\CRUDView',
                'title'     => Yii::t('page', 'View page'),
                'modelName' => $this->modelName,
            ],
            'delete' => [
                'class'     => 'backend\modules\core\components\CRUDDelete',
                'modelName' => $this->modelName,
            ],
            'connector'   => array(
                'class'   => ConnectorAction::className(),
                'settings'  => array(
                    'root'  => Yii::getAlias('@frontend/web/') . call_user_func([$this->modelName, 'getFilePath']),
                    'URL'   => '/' . call_user_func([$this->modelName, 'getFilePath']) . '/',
                    'rootAlias'   => 'Home',
                    'mimeDetect'  => 'none'
                )
            ),
        ];
    }

    public function getColumns()
    {
        return [

            $this->getGridSerialColumn(),
            ['attribute'  => 'uri'],
            ['attribute'  => 'header'],
            ['attribute'  => 'display_order'],
            $this->getGridActive(),
            $this->getGridActions(),

        ];
    }
    
}
