<?php

use yii\db\Migration;

class m160610_133200_table_static_pages extends Migration
{
    public function safeUp()
    {
        // таблица статических страниц страниц
        $this->createTable('{{%page_static_pages}}', [
            'id'              => $this->primaryKey(),
            'parent_id'       => $this->integer(11)->defaultValue(0),
            'uri'             => $this->string(100)->notNull(),
            'full_uri'        => $this->string(255)->notNull(),
            'header'          => $this->string(255)->notNull(),
            'menu_class'      => $this->string(255)->defaultValue(NULL),
            'content'         => $this->text()->defaultValue(NULL),
            'display_order'   => $this->integer()->defaultValue(0),
            'active'          => $this->smallInteger(1)->defaultValue(0),
        ]);

        $this->createIndex('uix_pages_static_page_uri', '{{%page_static_pages}}', 'uri', TRUE);
        $this->createIndex('uix_pages_static_page_full_uri', '{{%page_static_pages}}', 'full_uri', TRUE);
    }

    public function safeDown()
    {
        $this->dropIndex('uix_pages_static_page_full_uri', '{{%page_static_pages}}');
        $this->dropIndex('uix_pages_static_page_uri', '{{%page_static_pages}}');
        $this->dropTable('{{%page_static_pages}}');
    }

}
