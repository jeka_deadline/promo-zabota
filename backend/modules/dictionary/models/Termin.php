<?php
namespace backend\modules\dictionary\models;

use Yii;
use common\models\dictionary\Termin as BaseTermin;
use yii\helpers\ArrayHelper;

class Termin extends BaseTermin
{

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['display_order'], 'default', 'value' => 0],
            ]
        );
    }

    public function getFormElements()
    {
        return [
            'name'          => ['type' => 'text'],
            'text'          => ['type' => 'textarea'],
            'display_order' => ['type' => 'text'],
            'active'        => ['type' => 'checkbox'],
        ];
    }

    public function getViewAttributes()
    {
        return ['id', 'name', 'text', 'display_order', 'active'];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Вопрос',
            'text' => 'Ответ',
            'display_order' => Yii::t('core', 'Display order'),
            'active' => Yii::t('core', 'Active'),
        ];
    }

}