<?php
    return [
        'Termins'       => 'Словарь терминов',
        'Add termin'    => 'Добавить термин',
        'Update termin' => 'Обновить термин',
        'View termin'   => 'Просмотр термина',
        'List questions' => 'Список ЧаВо вопросов',
        'Add question answer' => 'Добавить вопрос/ответ',
        'View question answer' => 'Вопрос/Ответ',
        'Update question answer' => 'Обновить вопрос/ответ',
    ];
?>