<?php

use yii\db\Migration;

class m170130_090519_dictiondary extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%dictionary_termins}}', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(100)->notNull(),
            'text'          => $this->text()->notNull(),
            'display_order' => $this->integer(1)->defaultValue(0),
            'active'        => $this->smallInteger(1)->defaultValue(0),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%dictionary_termins}}');
    }

}
