<?php
use yii\widgets\DetailView;
use yii\helpers\Html;
?>

<?= Html::a(Yii::t('core', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>

<br>
<br>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'is_admin',
        'email',
        [
            'label' => Yii::t('user', 'Access to admin panel'),
            'value' => ($model->is_admin) ? Yii::t('user', 'Allowed') : Yii::t('user', 'Forbidden'),
        ],
        [
            'label' => Yii::t('user', 'Is user block'),
            'value' => ($model->blocked_at) ? Yii::t('core', 'Yes') : Yii::t('core', 'No'),
        ],
        [
            'label' => Yii::t('user', 'Is user confirm email'),
            'value' => ($model->confirm_email_at) ? Yii::t('core', 'Yes') : Yii::t('core', 'No'),
        ],
        [
            'label' => Yii::t('user', 'Is user confirm phone'),
            'value' => ($model->confirm_phone_at) ? Yii::t('core', 'Yes') : Yii::t('core', 'No'),
        ],
        'profile.surname',
        'profile.name',
        'profile.phone',
        'profile.address',
        'profile.date_birth',
        'profile.city',
        'profile.region',
    ],
]); ?>