<?php
use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use backend\modules\core\models\Helper;
?>

<?= Html::a(Yii::t('user', 'Create user'), ['create'], ['class' => 'btn btn-success']); ?>

<br>
<br>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'attribute' => 'is_admin',
            'filter'    => ['0' => Yii::t('core', 'No'), '1' => Yii::t('core', 'Yes')],
            'value' => function($model){ return ($model->is_admin) ? Yii::t('core', 'Yes') : Yii::t('core', 'No');},
        ],
        [
            'attribute' => 'email',
        ],
        [
            'attribute' => 'blocked_at',
            'value' => function($model){ return ($model->blocked_at) ? Yii::t('core', 'Yes') : Yii::t('core', 'No');},
            'filter' => Helper::getListYesNo(),
        ],
        [
            'format'    => 'raw',
            'header'    => Yii::t('user', 'Security actions'),
            'value'     => function($model) use ($currentUserId)
            {
                if ($model->id === $currentUserId) {
                    return FALSE;
                }

                $optionsRole = $optionsBlock = [
                    'class' => 'btn btn-xs',
                ];

                if ($model->is_admin) {
                    $textBtnRole  = Yii::t('user', 'Remove admin role');
                    $classBtnRole = 'btn-danger';
                } else {
                    $textBtnRole  = Yii::t('user', 'Add admin role');
                    $classBtnRole = 'btn-success';
                }

                if ($model->blocked_at) {
                    $textBtnBlock  = Yii::t('user', 'Unblock user');
                    $classBtnBlock = 'btn-danger';
                } else {
                    $textBtnBlock  = Yii::t('user', 'Block user');
                    $classBtnBlock = 'btn-success';
                }

                Html::addCssClass($optionsRole, $classBtnRole);
                Html::addCssClass($optionsBlock, $classBtnBlock);

                return
                    '<div class="btn-group">' .
                        Html::a($textBtnRole, Url::toRoute(['/user/user/update-user-role', 'id' => $model->id]), $optionsRole) .
                        Html::a($textBtnBlock, Url::toRoute(['/user/user/update-user-block', 'id' => $model->id]), $optionsBlock) .
                    '</div>';
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => Yii::t('core', 'Actions'),
        ],
    ],
]);