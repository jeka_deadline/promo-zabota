<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>

<?= $headerContent; ?>

<div class="crud-view">
    <p>
        <?php if ($showUpdateButton) : ?>

            <?= Html::a(Yii::t('core','Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?php endif; ?>
        <?php if ($showDeleteButton) : ?>

            <?= Html::a(Yii::t('core', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('core', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>

        <?php endif; ?>

    </p>

    <?= DetailView::widget([
        'model'       => $model,
        'attributes'  => $model->getViewAttributes(),
    ]) ?>

</div>

<?= $footerContent; ?>