<?php
use backend\assets\PrismaAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use backend\modules\core\widgets\AdminMenu\AdminMenu;
use yii\helpers\Url;

PrismaAsset::register($this);
?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link type="image/x-icon" href="favicon.png" rel="shortcut icon"/>
        <link href='' rel='stylesheet' type='text/css'>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody(); ?>
        <div id="theme-wrapper">
            <header class="navbar" id="header-navbar">
                <div class="container">
                    <a href="<?= Url::toRoute(['/']); ?>" id="logo" class="navbar-brand">Промо забота
                    </a>
                    <div class="clearfix">
                        <button class="navbar-toggle" data-target=".navbar-ex1-collapse" data-toggle="collapse" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars"></span>
                        </button>
                        <div class="nav-no-collapse navbar-left pull-left hidden-sm hidden-xs">
                            <ul class="nav navbar-nav pull-left">
                                <li>
                                    <a class="btn" id="make-small-nav">
                                    <i class="fa fa-bars"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="nav-no-collapse pull-right" id="header-nav">

                            <ul class="nav navbar-nav pull-right">
                                <li class="dropdown hidden-xs">
                                    <a href="<?= Url::toRoute(['/user/security/logout']); ?>" data-method="post">Выход</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
            <div id="page-wrapper" class="container">
                <div class="row">
                    <div id="nav-col">
                        <section id="col-left" class="col-left-nano">
                            <div id="col-left-inner" class="col-left-nano-content">

                                <?= AdminMenu::widget(['template' => 'prisma']); ?>

                            </div>
                        </section>
                        <div id="nav-col-submenu"></div>
                    </div>
                    <div id="content-wrapper">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="content-header" class="clearfix">
                                            <div class="pull-left">
                                                <!-- <ol class="breadcrumb">
                                                    <li><a href="#">Home</a></li>
                                                    <li class="active"><span>Dashboard</span></li>
                                                </ol> -->
                                                <h1><?= Html::encode($this->title) ?></h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?= $content; ?>

                            </div>
                        </div>
                        <footer id="footer-bar" class="row">
                            <p id="footer-copyright" class="col-xs-12">
                                Powered by PRISM Theme.
                            </p>
                        </footer>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>