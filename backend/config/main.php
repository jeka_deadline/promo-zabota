<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'core'],
    'modules' => include( __DIR__ ) . '/modules.php',
    'defaultRoute' => '/core/index/index',
    'language' => 'ru',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl' => '/admin',
        ],
        'user' => [
            'identityClass' => 'backend\modules\user\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
            'loginUrl' => ['/user/security/login'],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/themes/prisma/',
                'baseUrl' => '@web/themes/prisma',
                'pathMap' => [
                    '@app/views' => '@app/themes/prisma/views',
                    '@app/modules' => '@app/themes/prisma/modules',
                    '@app/widgets' => '@app/themes/prisma/modules',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => '/core/index/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => include( __DIR__ ) . '/rules.php',
        ],
    ],
    'params' => $params,
];
