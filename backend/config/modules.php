<?php
return [
    'core' => [
        'class' => 'backend\modules\core\Module',
    ],
    'user' => [
        'class' => 'backend\modules\user\Module',
    ],
    'page' => [
        'class' => 'backend\modules\page\Module',
    ],
    'dictionary' => [
        'class' => 'backend\modules\dictionary\Module',
    ],
    'stock' => [
        'class' => 'backend\modules\stock\Module'
    ],
];
?>